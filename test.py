from app.PETParser import *
from app.ChipSeqParser import *
from networkx.readwrite import json_graph;
import json;
from joblib import Parallel, delayed;
import glob;
import math;
from app.TSSAnnotParser import *;
from bx.intervals.intersection import Interval;
from app.ParseChromInfo import *;
from app.ChrMesh import ChrMesh;

def ProcessChiSeqFile(file):    
    ChipSeq = ChipSeqParser(file)
    ChipSeq.parsePeaks();    
    return ChipSeq;


if __name__ == "__main__":
    parser = PETParser("D:/temp/K562_5orMore.txt");
    parser.ReadPET();     
    #parser.updateInteractions() 
    #chromosoms = sorted( parser.Interheatmap["chromosome"].keys(), key= lambda x: int(x));
    #allChromNames = [i for i in range(1,parser.Interheatmap["interactions"].shape[0]+1)];
    #results = {"graph": json_graph.node_link_data( parser.interactions), 
    #                   "frequencies": [ {"chr": k, "freq" : parser.Interheatmap["chromosome"][k] } 
    #                                   for k in  chromosoms] ,
    #                   "heatmap": { "interactions" : parser.Interheatmap["interactions"].tolist(),
    #                                "columnNames" : allChromNames}
    #                  };    
    ##x = json.dumps(results);

    #ROOT= "C:/Users/sirus/Documents/Visual Studio 2012/Projects/PETVisualizer/PETVisualizer/static/Peaks/K562/"

    #bedfiles = [ROOT+f for f in os.listdir(ROOT) if f.endswith(".bed") ];
    
    #ChipPeaks = {};
    #for f in bedfiles:
    #    print("Processing "+ f)
    #    ChipPeaks[f] = ProcessChiSeqFile(f);        
    
    #results = Parallel(n_jobs=4) (delayed(ProcessChiSeqFile) (ROOT + f) for f in bedfiles);
    #x=1
    #chrInfo =ParseChromInfo("hg19");
    #chrs = chrInfo.ParseChromLengths()
    #TSSAnnot  = TSSAnnotParser("hg19");
    #TSSAnnot.GetAnnotations();
    #region = Interval(32554,36481,strand=-1,chrom="chr1")
    #res =TSSAnnot.GetNearByTSS(region);
    #print(res);
    #ChipSeq = ChipSeqParser("C:/Users/sirus/Documents/Visual Studio 2012/Projects/PETVisualizer/PETVisualizer/static/Peaks/K562/K562_H2A.Z_peaks.bed")
    #ChipSeq.parsePeaks();
    #Inregion = ChipSeq.getpeaksInRegion("chr1",109050968-1000,174077689+10000)
    #print(Inregion)
    
    filePath = "./tmp/3d_yeast.txt";
    models = ChrMesh();
    models.loadCoordinate(filePath);
    models.Triangulize()
    info = {"metadata":{
            "Version": 1,
            "type": "geometry",
            "generator": "3CPET-Viewer"
        },
            "vertices": models.ChromPoints['1'].flatten().tolist(),
            "faces": models.triangles['1'].simplices.flatten().tolist()
    }
    
    print(info)





