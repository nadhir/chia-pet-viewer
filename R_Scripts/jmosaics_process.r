library(jmosaics)
## Correct some errors in the code of the package
source("readBins.R")

readBins <- readBins2
input<-"~/tmp/Control/wgEncodeBroadHistoneK562ControlStdAlnRep1.sam_fragL200_bin200.txt"
H3K4me1 <- "~/tmp/Control/wgEncodeBroadHistoneK562H3k4me1StdAlnRep1.sam_fragL200_bin200.txt"
H3K4me3 <- "~/tmp/Control/wgEncodeBroadHistoneK562H3k4me3StdAlnRep1.sam_fragL200_bin200.txt"
H3K27ac <- "~/tmp/Control/wgEncodeBroadHistoneK562H3k27acStdAlnRep1.sam_fragL200_bin200.txt"
H2AZ <- "~/tmp/Control/wgEncodeBroadHistoneK562H2azStdAlnRep1.sam_fragL200_bin200.txt"

mappbility <- "~/tmp/map_fragL200_bin200/map_fragL200_bin200.txt"
GCcontent  <- "~/tmp/GC_fragL200_bin200/GC_fragL200_bin200.txt"
NNcontent  <- "~/tmp/N_fragL200_bin200/N_fragL200_bin200.txt"


H3K4me1_bin <- readBins(type=c("chip","input","M","GC","N"), fileName=c(H3K4me1, input,mappbility, GCcontent, NNcontent))
H3K4me3_bin <- readBins(type=c("chip","input","M","GC","N"), fileName=c(H3K4me3, input,mappbility, GCcontent, NNcontent))
H3K27ac_bin <- readBins(type=c("chip","input","M","GC","N"), fileName=c(H3K27ac, input,mappbility, GCcontent, NNcontent))
H2AZ_bin    <- readBins(type=c("chip","input","M","GC","N"), fileName=c(H2AZ, input,mappbility, GCcontent, NNcontent))

signal_Names <- c("H3K4me1","H3K4me3", "H3K27ac", "H2AZ")
signals <- list(H3K4me1_bin, H3K4me3_bin, H3K27ac_bin, H2AZ_bin)

cat("bins <- readBinsMultiple( signals)")
bins <- readBinsMultiple( signals)

fits <- list()

for(i in 1:length(bins)){
	 print(paste("mosaicsFit for bin",i))
     fits[[i]] <- mosaicsFit(bins[[i]], analysisType = "IO")
	 post1 <-jmosaics:::.mosaics_post(fits[[i]])
	 
	 tmp <- data.frame(fits[[i]]@chrID,fits[[i]]@coord, fits[[i]]@coord+ 200,
						post1$MDZ0, post1$MDZ1 + post1$MDZ2)
	 fname <- file.path(getwd(), signal_Names[i])
	 dir.create(signal_Names[i], showWarnings= TRUE)	 
	 fname <- file.path(fname, "signal_fit.txt")
	 print(paste("writing", fname))
	 write.table(tmp, file=fname,  col.names=FALSE, row.names=FALSE, quote=FALSE)	 
}

#print("Running jmosaicsPattern")
#result <- jmosaicsPattern(fits, region_length=1, FDR=0.01, thres=c(10,10), type=c('B','E','Pattern'), patternInfo='FALSE')

print("Saving results")
save(result, file ="~/tmp/jmosaics_Results.RData")

print("Done!")
