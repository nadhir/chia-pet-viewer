from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize
import numpy as np    


ext_modules = [Extension("jmosaics", ["jmosaics.pyx"])] #,extra_compile_args=["-Zi", "/Od"])] #,
						#extra_link_args=["-debug"])]

setup(
  name = 'jmosaics',
  cmdclass = {'build_ext': build_ext},
  include_dirs = [np.get_include()],         
  ext_modules = cythonize(ext_modules)
)