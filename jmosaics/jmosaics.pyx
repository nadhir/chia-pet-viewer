import numpy as np
cimport numpy as np
from libc.stdlib cimport free
from libc.math cimport pow nogil

DTYPE=np.double

ctypedef np.double_t  DTYPE_t


## the existing abs function only support int, 
## so we define the dabs function.

cdef inline double dabs(double x) nogil:
    if x >0 :
        return x
    else:
        return -x

cdef inline double dmax(double x, double y) nogil:
    if(x > y):
        return x
    else :
        return y

cdef class jmosaics:
    
    cdef public double[:,::1] p0, p1    
    cdef public double[:] p_B
    cdef public double[:,:] p_E
    cdef public double pi
    cdef public double[:] eta   
    cdef public double[:] a
    cdef public double[:,:] b
    cdef int maxiter #= 100
    cdef double epsilon  #= 0.0001

        
    def __cinit__(jmosaics self, int iter):
        self.maxiter = iter;
        self.epsilon = 1e-6		
        
    #def __dealloc__(jmosaics self):
    #   free(self.p_B)      
    #   free(self.p_E)  
    #   free(self.p1)       
    #   free(self.p0)

    
    cdef double prod(jmosaics self, double[:] signal, int start, int end) nogil:
        cdef int i
        cdef double p = 1
        for i in range(start,end):
            p = p * signal[i]       
        return p
        
    ## added d, for speed as it is faster then slicing the original matrix   cdef public double
    def prod2d(jmosaics self,double[:,:] signal, int start, int end,int d) nogil: 
        cdef int i
        cdef double p = 1
        for i in range(start,end):
            p = p * signal[d,i]     
        return p

        
        
    def f1(jmosaics self, double[:,:] forground, double[:,:] background, int start, int end, int d):
            
        cdef int s,v
        cdef double p1 =0       
        cdef double L = end - start     
        for v in range(1,(<int>L+1)):
            for s in range(0, (<int>L-v+1)):                                
                p1 += 1/(L *(L-v+1)) *  self.prod2d(background, start, start + s, d) * self.prod2d(forground,start + s, start + s+ v,d) * self.prod2d(background,start + s+  v , end, d)                
        
        return p1

    cdef void getRegionInitEnrich(jmosaics self,int[:] region_Sizes, double[:,:] background,double[:,:] foreground):
        
        ## TODO: need to check that:
        ##  - background and foreground have the same dimension
        ##  -  regions_sizes length equals the number of rows
        
        cdef int nbRegions = region_Sizes.shape[0]
        #print("we have %d regions" % nbRegions)
        
        ## the background and foreground signals of dimension D x L
        ## D : number of samples
        ## L : number of regions
        self.p0 = np.empty((background.shape[0], nbRegions))
        self.p1 = np.empty((background.shape[0], nbRegions))
                
        cdef int i,j, sample
        cdef int pos
        cdef double[:] res = np.zeros(nbRegions, dtype=np.double)
            
        for d in range(background.shape[0]):
            ## for each region calculate self.p0 and self.p1        
            pos =0
            for i in range(nbRegions):
                if(region_Sizes[i] == 1):
                    self.p0[d,i] = background[d,i]
                    self.p1[d,i] = foreground[d,i]
                else:
                    self.p0[d,i] = self.prod2d(background,pos, pos + region_Sizes[i], d)
                    self.p1[d,i] = self.f1(foreground,background, pos, pos + region_Sizes[i], d)   #self.prod2d(foreground,pos, pos + region_Sizes[i], d)               
                pos += region_Sizes[i]
                
        
        
    cdef void EM(jmosaics self,int[:] region_Sizes, double[:,:] background,double[:,:] foreground):

        

        self.getRegionInitEnrich(region_Sizes,background,foreground)
        
        cdef int nbSamples = <int> self.p0.shape[0]     
        cdef int nbRegions = <int> self.p0.shape[1]
        
        #print("We have %d samples and %d regions" % (nbSamples, nbRegions))
        
        self.eta = np.full(nbSamples,0.5)   
        self.pi = 0.02
            
        ## initialise the parameters
        cdef int iter = 0       
        cdef double[:] prev_eta = np.full(nbSamples, 0.5)
        cdef double prev_pi = 0.02
        cdef double max_diff =1

        cdef double[:] a = np.zeros(nbRegions)
        cdef double[:,:] b = np.zeros((nbSamples, nbRegions))
        
        cdef double[:] s1 
        cdef double[:] s2 
        
        cdef double[:] tmp = np.zeros(nbRegions)
        cdef double sum =0
        cdef double eta_d = 0   
        ## Do the EM algorithm
        while( (max_diff > self.epsilon) and (iter <= self.maxiter) ):
            #print("iterations %d" % iter)
            ## E-Step:          
            ## calculate the nominator and the denominator part of a        
            s1 = np.full(nbRegions, 1)
            s2 = np.full(nbRegions, 1)
            
            for d in range(nbSamples):              
                for i in range(nbRegions):
                    #if i < 10 :
                    #    print("(d,i) = ( %d, %d)" % (d,i))
                    #    print("s1[i]: %f, s2[i]: %f, eta[d]: %f, p0[d,i]: %f, p1[d,i]: %f" % (s1[i], s2[i], self.eta[d], self.p0[d,i], self.p1[d,i]))                  
                    s1[i]= s1[i]* ( self.eta[d] * self.p1[d,i] + (1-self.eta[d]) * self.p0[d,i] )
                    s2[i]= s2[i] * self.p0[d,i]                 
                    #if i < 10 :
                    #    print("after: s1[i]: %f, s2[i]: %f" % (s1[i], s2[i]))
            
            ## calculate a
            sum_a = 0
            for i in range(nbRegions):
                a[i] = (self.pi * s1[i])/ (self.pi * s1[i] + (1-self.pi)* s2[i])
                ## M-Step
                sum_a += a[i]                           
                        
            ## calculate b 
            for d in range(nbSamples):
                sum_b =0
                for i in range(nbRegions):
                    b[d,i] = (self.eta[d] * self.p1[d,i] * a[i])/ ( self.eta[d] * self.p1[d,i] + (1- self.eta[d])* self.p0[d,i])
                    ## M-Step
                    sum_b += b[d,i]                 
                self.eta[d] = sum_b/sum_a
                if( dabs(self.eta[d]  - prev_eta[d]) < max_diff):
                        max_diff = dabs(self.eta[d]  - prev_eta[d]) 
                    
            ## M-Step:                                                          
            self.pi = sum_a / nbRegions
            if( dabs(self.pi - prev_pi) < max_diff):
                max_diff = dabs(self.pi - prev_pi)
            
            #print("********************************")
            #print("iter: %d" % iter)
            #print("eta")
            #print(np.asanyarray(self.eta))
            #print("b")
            #print(np.asanyarray(b)[0:nbSamples, 0:10])
            #print("pi : %f" % self.pi)
            #print("a")
            #print(np.asanyarray(a)[0:10])
            #print("********************************")
            iter += 1
            prev_eta[:] = self.eta
            prev_pi = self.pi
        
        ## Use the estimated parameter to calculate the final probabilities:
        
        ## P(B)
        
        self.p_B = np.zeros(nbRegions)
        s1 = np.full(nbRegions, 1)
        s2 = np.full(nbRegions, 1)
        
        for d in range(nbSamples):              
            for i in range(nbRegions):                          
                s1[i]= s1[i]* ( self.eta[d] * self.p1[d,i] + (1-self.eta[d]) * self.p0[d,i] )
                s2[i]= s2[i] * self.p0[d,i]                 

        #print("s1 = %s" % str(np.asanyarray(s1)))
        #print("s2 = %s" % str(np.asanyarray(s2)))
        
        #print("p1 = %s" % str(np.asanyarray(self.p1)))
        #print("p0 = %s" % str(np.asanyarray(self.p0)))
        #print("a = %s" % str(np.asanyarray(a)))
        
        self.a = a
        self.b = b
        for i in range(nbRegions):
            self.p_B[i] = (self.pi * s1[i]) / (self.pi * s1[i] + (1-self.pi) * s2[i])
            
        self.p_E = np.zeros( (nbSamples, nbRegions))
        
        for d in range(nbSamples):
            for i in range(nbRegions):
                self.p_E[d,i] = (self.p_B[i] * self.eta[d] * self.p1[d,i] * a[i])/(self.eta[d] * self.p1[d,i] + (1-self.eta[d])* self.p0[d,i])
                
        #print("finished in %d iterations, diff= %d" % (iter,max_diff))

    def getPattern(jmosaics self,int[:] region_Sizes, double[:,:] background,double[:,:] foreground):
        
        ## First run the EM algorithm
        self.EM(region_Sizes, background, foreground)
        
        ## Get the pattern from the posterior probabilities 
        
        cdef int nbSamples = <int>self.p_E.shape[0]
        cdef int nbRegions = <int>self.p_B.shape[0]
        
        cdef int i, d
        cdef double s0,s1,s2
        cdef double z
        cdef double[:,:] pattern = np.zeros( (nbSamples, nbRegions) )
        if( (nbSamples > 0) and (nbRegions > 0)):
            for i in range(nbRegions):
                s0 = s1 = 1             
                z = pow(self.p_B[i], nbSamples-1)
                for d in range(nbSamples):
                    s0 = s0 * (self.p_B[i]- self.p_E[d,i])                  
                    s1 = s1 * (dmax(self.p_E[d,i], self.p_B[i]- self.p_E[d,i]))                                 
                    
                s0 = s0/z  + 1- self.p_B[i] # p(E_i = 0)
                s1 = s1/z                   # p(E_i = 1)            
                
                if(s1 > s0):
                    for d in range(nbSamples):
                        if( self.p_E[d,i] > (self.p_B[i]- self.p_E[d,i])):
                            pattern[d,i] = 1
        return np.asanyarray(pattern)