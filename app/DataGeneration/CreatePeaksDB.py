
from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, Integer, String, DECIMAL, FLOAT, BIGINT
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
import os;
import re;

engine = create_engine('sqlite:///./static/db/bio4.db', echo=False)
Base = declarative_base()
session= None

class Genome(Base):
    __tablename__ = "genome"
    id= Column(Integer, primary_key= True)
    name = Column("name",String)
    cellLines = relationship('CellLine')
    #bins = relationship('Bin')

class EnrichedFor(Base):
    __tablename__="EnrichedFor"
    enrichID = Column(Integer, primary_key=True)
    cellLineID = Column(Integer, ForeignKey('CellLine.id'))
    binID = Column(Integer, ForeignKey('Bin.id')) 
    signalID = Column(Integer, ForeignKey('SingalType.id'))
    background = Column("background", FLOAT)
    foreground = Column("foreground", FLOAT)    


    #signals = relationship('SignalType', uselist=True)

class CellLine(Base):
    __tablename__ = "CellLine"
    id = Column(Integer, primary_key = True)
    name = Column("name",String(15))    
    genomeId  = Column(Integer, ForeignKey("genome.id"))

class SignalType(Base):
    __tablename__ = "SingalType"
    id= Column(Integer, primary_key= True)
    name = Column("name",String(40))

class Bin(Base):
    __tablename__ = "Bin"
    id = Column(Integer, primary_key=True)
    chr = Column("chr",String(5))
    start = Column("start",BIGINT)
    end = Column("end",BIGINT)
    genomeId  = Column(Integer, ForeignKey("genome.id"))    
    #cellLine = Column(Integer,ForeignKey("CellLine.id"))
    #signalType = Column(Integer, ForeignKey("SingalType.id"))
    #score = Column("score", DECIMAL)
    #qvalue = Column("qvalue",DECIMAL)    


class TSS(Base):
    __tablename__ = "TSS"
    id = Column(Integer, primary_key = True)
    genomeID = Column("genomeID", ForeignKey("genome.id"))
    chr = Column("chr",String(5))
    start = Column("start",BIGINT)
    end = Column("end",BIGINT)
    strand = Column("strand",Integer)
    ensemblID = Column("ensemblID",String)
    HGNCID = Column("HGNCID",Integer)
    HGNCSymb = Column("HGNCSymb",String)
    @hybrid_method
    def intersects(self,interval, upStrm, downStrm):
        muA = (int(interval.start) + int(interval.end))/2
        roA =  muA - int(interval.start)
        if(interval.strand == -1 or interval.strand == "-"):
            muB = (self.end + upStrm +  self.end - downStrm)/2
            roB = muB - self.end + downStrm
        else:
            muB = (self.start - upStrm +  self.start + downStrm)/2
            roB = muB - self.start + upStrm
        gapRatio = abs(muA - muB)/ (roA + roB)
        return garRation < 1
      




def populateGenome(path):
    #Get the list of available genomes
    Availablegenomes = [name for name in os.listdir(path) if os.path.isdir(path+"/"+name)]
    
    for genome in Availablegenomes :
        g = Genome(name=genome)
        session.add(g)
    session.commit()
    return None

def PopulateTSS(path):
    print("Populating TSS table")
    try:
        #Get the list of available genome from the database    
        genomes = session.query(Genome).all()
    except Exception as e:
        raise e

    #For each genome get the TSS file and populate it into the database
    for genome in genomes :
        tssPath = path + "/%s/%s.tss" % (genome.name,genome.name)
        if(os.path.exists(tssPath)):
            nrow=0
            with(open(tssPath, "r")) as TSSs :
                #Parse each line
                for tss in TSSs:
                    #We ignore comments
                   if(re.match("#", tss) is not None ) :                    
                       continue;
                   tss = tss.strip()
                   TSSInfo = re.split("\s+", tss);
                   if(len(TSSInfo) >= 8):
                       entry = TSS(genomeID=genome.id,chr= TSSInfo[0],start=TSSInfo[1], end= TSSInfo[2],strand= TSSInfo[5],
                                  ensemblID=TSSInfo[4], HGNCID= TSSInfo[6] ,HGNCSymb = TSSInfo[7] )
                       session.add(entry)
                       nrow+=1
                   #each 100 line we commit
                   if(nrow%10000 ==0):
                       session.flush()
                       session.commit()
            #commit the rest if any
            session.flush()
            session.commit()


def PopulateCellLine(path):
    print("Populating CellLine table")
    try:
        #Get the list of available genome from the database    
        genomes = session.query(Genome).all()
    except Exception as e:
        raise e

    for genome in genomes:
        genomePath = path + "/" + genome.name
        AvailableCellLines = [name for name in os.listdir(genomePath ) if os.path.isdir(genomePath + "/" + name)]        
        for cellLine in AvailableCellLines :            
            cl = CellLine(name=cellLine)
            session.add(cl)
            genome.cellLines.append(cl)

        session.commit()
    return None

def PopulateSginalType(path) :
    print('Populating SignalType Table')
    signals = [name for name in os.listdir(path) if name == "signals.txt"]
    if len(signals) == 1:       
       with open(path + "/"+ signals[0]) as f:
           # each signal type should be in a line
           # the order in which they apear is the one used for the pattern 
           content = f.readlines(); 
       for type in content :
           #signalName = re.sub("\n","",type)   
           signalName = type.strip()
           signal = SignalType(name=signalName)
           session.add(signal)

       session.commit()

def PopulateBins(path):
    print('Populating bins')
    try:
        #Get the list of available genome from the database    
        genomes = session.query(Genome).all()
    except Exception as e:
        raise e

    #For each genome get the TSS file and populate it into the database
    for genome in genomes :
        print("populating peaks for genome %s" % genome.name)
        binsPath = path + "/%s/peaks.bed" % (genome.name)
        if(os.path.exists(binsPath)):
            nrow=0
            with(open(binsPath, "r")) as bins :
                #Parse each line
                for bin in bins:
                    #We ignore comments
                   if(re.match("#", bin) is not None ) :                    
                       continue;
                   #bin = re.sub("\n","",bin)
                   bin = bin.strip()
                   BinInfo = re.split("\s+", bin);

                   if(len( BinInfo) >= 3):
                       entry = Bin(chr= BinInfo[0] , start=  BinInfo[1], end= BinInfo[2])                       
                       session.add(entry)
                       #genome.bins.append(entry)
                       nrow+=1
                   #each 100 line we commit
                   if(nrow%1000000 ==0):
                       print("nrow = %d, last inserted bin is : %s %s %s" % (nrow, BinInfo[0], BinInfo[1], BinInfo[2]))
                       session.flush()
                       session.commit()
            #commit the rest if any
            print("The last commit")
            session.flush()
            session.commit()
            
## TODO: Finish this one           
def PopulateEnrichment(path):

     try:
        ## Get the list of available genome from the database    
        genomes = session.query(Genome).all()
        ## Get the list of the available signals
        signals = session.query(SignalType).all()
     except Exception as e:
        raise e

     for genome in genomes :
        ## for all the available cell lines for this genome                     
        for cellLine in genome.cellLines:

            for signal in signals :
                peaksFolder = path + "/%s/%s/processed/%s/" % (genome.name, cellLine.name, signal.name)                
                peaksFiles = [file for file in os.listdir(peaksFolder) if file.endswith("signal_fit.txt")]
                if(len(peaksFiles) == 1):
                    peaksFiles = peaksFiles[0]    
                    with open(peaksFolder+"/"+ peaksFiles, "r") as peaks:
                        rowNum = 0
                        for peak in peaks:
                            if( re.match("#", peak) is not None) : 
                                continue
                            #peak = re.sub("\n","",peak)
                            peak = peak.strip()
                            peakInfo = re.split("\s+",peak)

                            if(len(peakInfo)>= 5):
                                ## checK if the bin exists in the data base
                                ## NOTE: Maybe find a better way to do it.
                                bin = session.query(Bin).filter(Bin.chr == peakInfo[0], Bin.start == peakInfo[1], Bin.end== peakInfo[2]).first()
                            
                                if bin != None:
                                    entry = EnrichedFor(cellLineID = cellLine.id, binID= bin.id, signalID = signal.id,background = peakInfo[3], foreground = peakInfo[4])
                                    session.add(entry)
                                    rowNum +=1
                                    ## check if the pattern contains all the signals
                                    #if(len(peakInfo[3])== len(signals)):
                                    #    if "1" in peakInfo[3]:
                                    #        ## if the Bin is enriched for the pattern add it 
                                    #        ## to the EnrichedFor table                                        
                                    #        for i in range(0, len(signals)):                                                                                        
                                    #            if(peakInfo[3][i] == "1"):                                                
                                    #                entry = EnrichedFor(cellLineID = cellLine.id, binID= bin.id, signalID = signal.id, )
                                    #                session.add(entry)
                                    #                ## Add the corresponding signals
                                    #                #entry.signals.append(signals[i])
                                    #                rowNum +=1
                                    #else:
                                    #    EmptyArgs("The bin %s %s %s pattern %s has a diffrent number of signals as the one available" % peakInfo)
                                else:
                                      EmptyArgs("It seems that chIP-Seq signal is not binned correctly \n line %s %s %s %s" % peakInfo)
                                
                            if(rowNum % 100000 ==0):
                                print("nrow= %s, last inserted bin = %s %s %s" % (rowNum, peakInfo[0], peakInfo[1], peakInfo[2]))                                
                                session.flush()
                                session.commit()
                                
                    #Commit the rest if any
                    session.commit()

     return None

Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()
#session._model_changes = {}

genomesPath = "./static/Annotations"
cellLinesPath ="./static/Peaks"

populateGenome(genomesPath)

PopulateCellLine(cellLinesPath)
PopulateSginalType(cellLinesPath)
PopulateBins(genomesPath)
print("Creating Index for table bin, it may take some time ..... ")
Index('idx_bins', Bin.chr, Bin.start, Bin.end).create(engine)
PopulateEnrichment(cellLinesPath)
print("Creating Index for table EnrichedFor, it may take some time, .....")
Index('idx_enrich', EnrichedFor.binID, EnrichedFor.cellLineID, EnrichedFor.signalID)

print("Creatting TSS table")
PopulateTSS(genomesPath)
print('Creating Index for TSS table, it may take some time ...')
Index("idx_tss", TSS.chr, TSS.start, TSS.end, TSS.strand).create(engine)

print("done!")
#try :
#except Exception as e:
#    print(e.with_traceback)







