from collections import defaultdict;
import os;
import re;


class ParseChromInfo(object):
    """description of class"""
       
    def __init__(self, genome):
        genomes = ["hg19","hg18"]
        if(genome in genomes) :
            self.file=  "./static/ChrInfo/%s/%s_chromInfo.bed" % (genome, genome)

    def ParseChromLengths(self):
        self.chroms = defaultdict(int)
        #check if the file exists
        try :
            with( open(self.file,"r") ) as chrInfo :
                for chr in chrInfo :
                    chr = re.sub("\n","",chr)
                    chr = re.split("\t",chr)
                    if(len(chr)>=2):
                        #Substitute all X and Y values
                        chr[0] = re.sub("chr","",chr[0])                        
                        chr[0] = re.sub("X","23",chr[0])
                        chr[0] = re.sub("Y","24",chr[0])
                        #Just to raise an error in case it is not an integer                        
                        self.chroms[int(chr[0])] = int(chr[1])
            return True;
        except :
            return None;
    

