from app import app
from flask import Flask, render_template, views, jsonify, Response, g, request, session
from werkzeug.utils import secure_filename
from builtins import Exception
from networkx.readwrite import json_graph;
from app.PETParser import PETParser;
from app.ChipSeqParser import ChipSeqParser
from app.ParseChromInfo import ParseChromInfo;
from app.TSSAnnotParser import TSSAnnotParser;
from app.ChrMesh import ChrMesh;
from collections import defaultdict, OrderedDict;
import uuid;
import json;
import os;  


class main(views.MethodView):
    def get(self):        
        return render_template("Index_1.html")



class getPETs(views.MethodView):

    def get(self):    
        if(session["filename"] is not None or len(str(session["filename"]))):    
            filePath = os.path.join(app.config['UPLOAD_FOLDER'],str(session["uid"]), str(session["filename"]))
            parser = PETParser(filePath); 
            parser.ReadPET();     
            parser.updateInteractions()    
            chrInfo = ParseChromInfo("hg19")
            chrInfo.ParseChromLengths();
            chromosoms = sorted( parser.Interheatmap["chromosome"].keys(), key= lambda x: int(x));
            allChromNames = [i for i in range(1,parser.Interheatmap["interactions"].shape[0]+1)];
            g.graph = parser.interactions
            results = { "status": "success",
                        "graph": json_graph.node_link_data( parser.interactions), 
                       "frequencies": [ {"chr": k, "freq" : parser.Interheatmap["chromosome"][k] } 
                                       for k in  chromosoms] ,
                       "heatmap": { "interactions" : parser.Interheatmap["interactions"].tolist(),
                                    "columnNames" : allChromNames},
                       "ChrInfo": {chr:length for (chr,length) in chrInfo.chroms.items()},
                       "inter" : json_graph.node_link_data( parser.transInteractions)
                      };    
            #return Response(json.dumps(json_graph.node_link_data( parser.interactions)), mimetype='application/json')        
            path = os.path.join(app.config['UPLOAD_FOLDER'],str(session["uid"]))        
            results = json.dumps(results);
            try:                                      
                fileName = os.path.join(path,"result.json")
                clusFile = os.path.join(path,"clusters.json")
                with open(fileName,"w+") as f:
                    f.write(results)
                #Save the clusters to another file so they will be uploaded by the table asychronesly
                #to let the user fet have an impression of the graphics first
                with  open(clusFile,"w+") as f:
                    res = {"draw":1, 
                           "recordsTotal": len( parser.clusInfo),
                           "recordsFiltered" : len(parser.clusInfo),
                           "data": parser.clusInfo};
                    f.write(json.dumps(res));

            except Exception as e :
               results = { "status": "error",
                           "msg" : "Error when processing"
                         }
               return Response(results,mimetype='application/json');

            return Response(results,mimetype='application/json');
        else:
            results = { "status": "error",
                           "msg" : "Couldn't find file one server"
                         }
            return Response(results,mimetype='application/json');

    def post(self):    
        
        file = request.files["file"]
        content = {"files": []}
        if(AllowedFiles(file)):            
            if("uid" not in session.keys() or session["uid"] is None or len(str(session["uid"])) ==0) :
                session["uid"] = uuid.uuid4()        

            filename = secure_filename(file.filename)   
            root = os.path.join(app.config['UPLOAD_FOLDER'],str(session["uid"]))
            uploadPath = os.path.join(root, filename)
            try:
                if not  os.path.exists(root):
                    os.mkdir(root)

                file.save(uploadPath)              
                session["filename"] = filename               
                content["files"] = {
                    "name": file.filename,
                    "size": request.content_length ,
                    "url": "",
                    "thumbnailUrl": "",
                    "deleteUrl": "",
                    "deleteType": "DELETE"
                    }
            except Exception as e:
                content["files"]= {
                    "name" : file.filename,
                    "size" : request.content_length ,
                    "error": "Error when loading the file"
                    }
        else:
            content["files"]= {
                    "name" : file.filename,
                    "size" : request.content_length ,
                    "error": "Only .txt and .bed files are accepted"
                    }
            
        return Response(json.dumps(content),mimetype='application/json')


def AllowedFiles(file):
    if file.filename.endswith(".txt") or file.filename.endswith(".bed"):
        return True
    else:
        return False

@app.route("/getGeneInfo/<genome>/<chr>/<strand>/<start>/<end>",methods=["GET","POST"]) 
def GetGeneInfo(genome,chr,strand,start,end):

    try:
        if(checkGeneInfoParam(genome,chr,strand,start,end)):

            tssParser = TSSAnnotParser(genome=genome, path=app.config['ANNOT'])

            region = {'region_start': int(start),
                      'region_end': int(end),
                      'chrom': 'chr'+chr,
                      'strand': '.'}
            results = tssParser.GetNearByTSS(region)
        else:
            results = {'error': "The specified info are not correct"}
    except:
        results = {'error': "Specified genome doesn't exist"}

    return Response(json.dumps(results), mimetype="application/json")



@app.route("/ChrInfo/Cytobands/<genome>")
def getCytobands(genome):    
    content = ''
    try:
        with(open("./static/ChrInfo/%s/cytobands.json" % genome, "r")) as cyto :
            content = cyto.read()
    except Exception as e:
        content = str(e)
    
    return Response(content,mimetype="application/json")

#@app.route("/getEnriched/", methods=["GET","POST"])

class getEnrichedRegions(views.MethodView):
    
    def get(self):        
        return ""

    def post(self):

        results = {"Enhancer": [], "Promoter": []}
        if request.json is not None:
            data = request.json
            results = {"Enhancer": [], "Promoter": []}
            if(data["nodes"] is not None):
                chipParser = ChipSeqParser(app.config['GENOMES'],
                                           binsize=app.config['BIN_SIZE'])

                try:
                    results = chipParser.getEnrichedRegions(data["cellLine"],
                                                            data["genome"],
                                                            data["Promoter"],
                                                            data["Enhancer"],
                                                            data["nodes"])
                except Exception as e:
                    pass
        return Response(json.dumps(results), mimetype="application/json")
       


@app.route("/getInitClusterList", methods=["GET"])
def getInitialCluster():
    sessionFolder = os.path.join(app.config['UPLOAD_FOLDER'],str(session["uid"]))
    clusFile = os.path.join(sessionFolder, "clusters.json")
    #Check if the file exsit
    if os.path.exists(clusFile) :
        #Load it
        try:
            with open(clusFile,"r") as f:
                content = f.read()                
            return Response(content, mimetype="application/json")  

        except Exception as e:
            print(e)
            return Response(json.dumps({"error": "Error when loading cluster"}), mimetype="application/json")   

    else: #if the file doesn't exist
        print("the file %s doesn't exist" % clusFile);
        return Response(json.dumps({"error": "Error when loading cluster"}), mimetype="application/json")   
    




app.add_url_rule("/", view_func = main.as_view("main"), methods=["GET"]);
app.add_url_rule("/getPets", view_func= getPETs.as_view("getPets"), methods=["GET","POST"])
app.add_url_rule("/getEnriched", view_func= getEnrichedRegions.as_view("getEnriched"), methods=["GET","POST"])



def getUploadedData():
    Filepath = "./tmp/%s/result.json" % session["uid"] 
    if(os.path.exists(Filepath)):
        with open(Filepath,"r") as f:
            content = f.read()
            return json.JSONDecoder().decode(content)
    else:
        return None

def checkGeneInfoParam(genome,chr, strand, start, end):
    
    try :

        genomePath = os.path.join(app.config['ANNOT'], genome, "%s.tss.gz" % genome)
        if not os.path.exists(genomePath):
            return False

        int(start);
        int(end);
        chr = int(chr);
        if(chr not in range(1,25)): return False;

        strand =int(strand);
        if(strand not in [1,-1]): return False;

        return True;
    except Exception as e:
        return False;
    
