from distutils.core import setup
from distutils.extension import Extension
import numpy as np

try :
  from Cython.Distutils import build_ext
  from Cython.Build import cythonize
except ImportError:
  use_cython = False
else:
  use_cython = True


cmdclass = {}
ext_modules = []

if use_cython:
    ext_modules += [
        Extension("jmosaics",
                  ["jmosaics.pyx"], )]
                  #extra_compile_args=["-g"],
                  #extra_compile_args=["-Zi", "/Od"],
                  #extra_link_args=["-g"])]
    cmdclass.update({'build_ext': build_ext})
else:
  ext_modules += [
    Extension('jmosaics',
              ['jmosaics.c'],
              )
  ]

setup(
    name='jmosaics',
    cmdclass=cmdclass,
    include_dirs=[np.get_include()],
    ext_modules=cythonize(ext_modules)
)
