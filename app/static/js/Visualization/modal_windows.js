/**
 * Created by Mohamed Nadhir Djekidel on 05/01/15.
 */



var WarningMessages =  (function($){

    var msgPanel;

    return{
        show : function(title, message, options){

            var settings = $.extend({

                autoclose : -1,
                theme : 'danger',
                position : 'center'
            }, options);

            msgPanel = $.jsPanel({
                        paneltype : 'hint',
                        theme : 'danger',
                        position : settings.position,
                        size : {'width' : $(document).width()/3, 'height': 'auto'},
                        content : "<div>" + message + "</div>"
                    }).css('box-shadow', '0 0 50px 20px rgba(64, 64, 64, 1)');
        },
        hide : function(){

            if(typeof msgPanel != "undefined"){
                msgPanel.close();
            }
        }

    }
});
