﻿$(document).ready(function () {


    function override(object, methodName, callback){

        object[methodName] = callback(object[methodName])
    }

    override(dalliance, 'showToolPanel', function(original){
        return function(panel, nowrap){

            var panel =$.jsPanel({
                title : "Browser configuration",
                overflow: 'scroll',
                id : 'config_browser',
                //selector : "#basicStat",
                content : panel,
                theme : 'light',
                position : 'center',
                size : {
                    width: function () {
                        return $(window).width() / 3
                    },
                    height: function () {
                        return $(window).height() / 4
                    }
                }
            });

        }
    } );

    $("body").on('jspanelclosed', function(evt, id){
        dalliance.setUiMode('none');
    })


    $("[qtip-val]").each(function () {
        $(this).qtip({
            style: { name: "dark" },
            content: {
                text: function (event, api) {
                    return $(this).attr("qtip-val");
                }
            }
        })
    }
   )
    

    $('button.petUpload').bind('click', function () {
        displayPET("/getPets");
        return false;
    });


    $("#fileupload").fileupload({
        url: "/getPets",
        dateType: 'json',
        maxNumberOfFiles: 1,
        acceptFileTypes: /(\.|\/)(txt|bed)$/i,
        add: function (e, data) {
            data.context = $("button.submitPet").click(function () {
                //ToDo: Display the modal window                                        
                //data.context = $('<span id="fileUploadRate">').text("0%").appendTo($(this))
                var jqXHR = data.submit()
            });
        },
        done: function (e, data) {
            $("button.petUpload").removeAttr("disabled");
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            //Diplay the Progress Bar
            $("div#fileuploadProgressBar").css("width", progress + "%")
            $("div.progress-bar-container").attr("qtip-val", progress);
            $("span#fileUploadRate").text(progress + "%")
        }
    })

    $("button.btnAnnotate").bind("click", function () {
        getAnnotation("/getEnriched");
    })

    frequencies = [];
    for (var chr = 1; chr < 25; chr++) {
        datum = { "chr": chr.toString(), "freq": 0 };
        frequencies.push(datum)
    }

    /*Load Chromosom colors*/
    d3.json("/ChrInfo/Cytobands/hg19", function (error, data) {
        chrCytobands = data.cytobands;
        chrCenters = data.centers;
    })

    //color = d3.scale.category20c();
    var col =["#1f78b4","#a6cee3","#b2df8a","#33a02c","#fb9a99","#e31a1c","#fdbf6f","#ff7f00","#cab2d6","#6a3d9a","#ffff99","#b15928",
             "#8dd3c7","#ffffb3","#bebada","#fb8072","#80b1d3","#fdb462","#b3de69","#fccde5","#d9d9d9","#bc80bd","#ccebc5","#ffed6f",
         ];
    color = d3.scale.ordinal().range(col);
    genome = "hg19";
    InitializeHist();
    InitializeGraph();    
});



function editGenomeBrowserUI() {

    var pullRightBtn = $("div.btn-toolbar > a.pull-right");
    var Toolbarcontainer = $("<div class='newToolbar'></div>").insertBefore(pullRightBtn)
    pullRightBtn.appendTo(Toolbarcontainer);

    $("div.btn-toolbar > div.pull-right").appendTo(Toolbarcontainer);
    $("i.fa-chevron-left").parent().css("float", "right").appendTo(Toolbarcontainer);

    //Change some layouts
    var browserTitle = $("div.btn-toolbar > div.title").detach();
    browserTitle.insertBefore("div.dalliance")
    //Arrange the size of the span so it will be on the same line
    $("div.btn-toolbar.toolbar > div.btn-group > span.btn > input").css("width", "inherit")

    var tmp= $("i.fa-eraser").parent().remove();
    tmp.insertBefore('.newToolbar');

    tmp = $(".loc-group").remove();
    tmp.insertBefore('.newToolbar');

}

function getAnnotation(url) {

    var Annot = {}
    Annot["Enhancer"] = getMarksValues("Enhancer");
    Annot["Promoter"] = getMarksValues("Promoter");
    Annot["cellLine"] = $("select.cellLines").val();
    Annot["genome"] = $("select.genomes").val();
    //Annot["nodes"] = svg.selectAll("circle.node").data()
    Annot["nodes"] = sig.graph.nodes()


    jQuery.ajax({
        url: url,
        type: 'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(Annot),
        success: function (data) {
            ColorAnnotatedNodes(data)
        },
    });
};


function ColorAnnotatedNodes(data) {

    if (data.hasOwnProperty("Promoter") && data.hasOwnProperty("Enhancer")) {


        sig.graph.nodes().forEach(function (d, i) {
            if (data.Promoter.indexOf(d.id) != -1) {
                d.color = '#ee006e';
                d.label = "Promoter";
            }
            else {
                if (data.Enhancer.indexOf(d.id) != -1) {
                    d.color = "#085aca";
                    d.label = "Enhancer";
                }
                else{
                    d.label= ""
                }
            }


        });
        sig.refresh();
        /*
        if(node.length == 0) return;
        //For each node
        node.each(function (d) {
            candidate = d3.select(this)
            //Check if it is an Enhancer
            for (var i = 0; i < data["Enhancer"].length; i++) {
                if (d.id == data["Enhancer"][i].id) {
                    candidate.attr("r", 10)
                             .attr("class", function () { return candidate.attr("class") + " " + "enhancer" })
                             .style("fill", "green")

                    candidate.select("text").html("Enhancer")

                }
            }
            //Check if it is a promoter 
            for (var i = 0; i < data["Promoter"].length; i++) {
                if (d.id == data["Promoter"][i].id) {
                    candidate.attr("r", 10)
                             .attr("class", function () { return candidate.attr("class") + " " + "promoter" })
                             .style("fill", "red")
                    candidate.select("text").html("Enhancer")
                }
            }
        })
        */
        var dtTable = $("table#clustersInfo").DataTable();

        //Do the stats and update table 
        IDs = dtTable.column(0).data()
        for (var i = 0; i < IDs.length; i++) {
            var clusSize = dtTable.row(i).data().size;
            var Eselector = sig.graph.nodes().filter(function(d) {
                return d.cluster == i & d.label == "Enhancer";
            });
            var Pselector = sig.graph.nodes().filter(function(d) {
                return d.cluster == i & d.label == "Promoter";
            });
            var NbEnhancer = Eselector.length
            var NbPromoter = Pselector.length

            dtTable.row(i).data().enhancer = NbEnhancer / clusSize;
            dtTable.row(i).data().promoter = NbPromoter / clusSize;                        
        }

        dtTable.rows().invalidate().draw();

    }
}

function getMarksValues(markType) {
    var selectedmarks = $("input[class=" + markType + "Mark][checked]")
    var marks = {}
    if (selectedmarks.length > 0) {
        //For each mark get the desired level
        for (var i = 0; i < selectedmarks.length; i++) {
            var name = markType + selectedmarks[i].value + "Level";
            var level = $("input[name=" + name + "][checked]").get(0)
            if (typeof (level) != "undefined") {
                if (level.value === "Hight") {
                    marks[selectedmarks[i].value] = 1
                }
                else {
                    marks[selectedmarks[i].value] = 0
                }
            }
        }
    }

    return marks;
}

function increase_brightness(hex, percent) {
    // strip the leading # if it's there
    hex = hex.replace(/^\s*#|\s*$/g, '');

    // convert 3 char codes --> 6, e.g. `E0F` --> `EE00FF`
    if (hex.length == 3) {
        hex = hex.replace(/(.)/g, '$1$1');
    }

    var r = parseInt(hex.substr(0, 2), 16),
        g = parseInt(hex.substr(2, 2), 16),
        b = parseInt(hex.substr(4, 2), 16);

    return '#' +
       ((0 | (1 << 8) + r + (256 - r) * percent / 100).toString(16)).substr(1) +
       ((0 | (1 << 8) + g + (256 - g) * percent / 100).toString(16)).substr(1) +
       ((0 | (1 << 8) + b + (256 - b) * percent / 100).toString(16)).substr(1);
}

displayPET = function (url) {

    d3.json(url, function (error, data) {

        if (data.status == "success") {
            //Update the chromosome frequencies        
            for (var i = 0; i < data.frequencies.length; i++) {
                var found = false;
                for (var j = 0; j < frequencies.length; j++) {
                    if (frequencies[j].chr === data.frequencies[i].chr) {
                        frequencies[j].freq = data.frequencies[i].freq
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    datum = { "chr": date.frequencies[i].chr, "freq": data.frequencies[i].freq };
                    frequencies.push(datum);
                }
            }

            chrLength = data.ChrInfo;

            chrColor = Array()
            chromInfo = Array()
            //Create a gradiant color scale for each chromosome
            for (var chr in data.ChrInfo) {
                if (data.ChrInfo.hasOwnProperty(chr)) {
                    chrColor[String(chr)] = d3.scale.pow().exponent(2)
                                  .domain([1, data.ChrInfo[String(chr)]])
                                  .range([increase_brightness(color(chr), 10), color(chr)])

                    chromInfo[String(chr)] = { "chrLength": chrLength[String(chr)], "bands": chrCytobands[String(chr)], "centers": chrCenters[String(chr)], "chr": String(chr) }

                }
            }



            graph = data.graph;
            graph.edges = graph.links;
            delete graph.links;
            heatmap = data.heatmap;

            if (typeof (graph) != "undefined") {                

                linkWidth = d3.scale.linear()
                      .range([2, 20])
                      .domain(d3.extent(graph.edges, function (l) { return parseInt(l.freq, 10); }));

                nodeMap = {};
                maxX = $('div#NETContainer').width();
                maxY = $('div#NETContainer').height();

                // Nodes occupy 60% of the sufface
                var nodeSize = (maxX * maxY * 0.6) * graph.nodes.length;
                graph.nodes.forEach(function (x, i) {
                    nodeMap[i] = x;
                    x.id = i.toString();
                    x.color = chrColor[x.chr](x.start);
                    //x.x = Math.random() * maxX;
                    //x.y = Math.random() * maxY;
                    x.size = nodeSize;
                    //x.label = "chr" + x.chr + ":" + x.start + "-" + x.end;
                });
              
                graph.edges.forEach(function (e, i) {
                    e.id = i.toString();
                    e.source = nodeMap[e.source].id.toString();
                    e.target = nodeMap[e.target].id.toString();
                })

                drawNetCanvas(graph, 'div#NETContainer');                
            };
            

            DrawCircosMap("div.InteractionsStat")
            if (typeof (frequencies) != "undefined") { displayHistogram(); };
            //if (typeof (heatmap) != "undefined") { displayHeatmap(); };

            // Display the statistics about the graph
            if ($("div.clusStatDisplay").length)
                CreateClustersStatPanel("div.clusStatDisplay");
        }
        else {
            alert(data.msg)
        }
    }
   );

    if ($("div.newToolbar").length == 0) {
        editGenomeBrowserUI();
    }
};

drawNetCanvas = function (graph, containerID) {

    var padding = 1.5, // separation between same-color nodes
        clusterPadding = 6, // separation between different-color nodes
        maxRadius = 12; 

    // Use the pack layout to initialize node positions.
    d3.layout.pack()
        .sort(null)
        .size([maxX, maxY])
        .children(function (d) { return d.values;})
        .value(function (d) { return d.size * d.size; })
        .nodes({ values: d3.nest()
                        .key(function (d) {return d.cluster;})
                        .entries(graph.nodes)
              });

    var n = graph.nodes.lengh, // total number of nodes
    m = Object.keys(chromInfo).length; // number of distinct clusters
    

    // contains the centers of each cluster
    clusters = new Array(m);

    var nodeSize = 2
    // Calculate the centers for each cluster
    // and keep just the useful tags of a node    
    graph.nodes.forEach(function (node, i) {

        if (!clusters[node.cluster]) {
            clusters[node.cluster] = {
                x: node.x,
                y: node.y,
                size: nodeSize
            }
        } else {
            clusters[node.cluster].x += node.x;
            clusters[node.cluster].y += node.y;
            clusters[node.cluster].size += 1;
        }

        graph.nodes[i] = {
            chr: node.chr,
            cluster: node.cluster,
            color: node.color,
            start: node.start,
            end: node.end,
            id: node.id,
            x: node.x,
            y: node.y,
            size: node.size
        }
    });

    d3.range(m).forEach(function (i) {
        if(typeof clusters[i] != 'undefined') {
            clusters[i].x = clusters[i].x / clusters[i].size;
            clusters[i].y = clusters[i].y / clusters[i].size;
        }
    });

    // Clear the existing graph if any
    if (typeof sig != 'undefined') {
        sig.graph.clear();
        sig.refresh();
    }

    // Render the graph
    sigma.renderers.def = sigma.renderers.canvas;
    sig = new sigma({
        graph : graph,
        container: 'NETContainer',
        settings: {
            drawEdges: true,
            minNodeSize : 2,
        }
    });
       
    // We display the info of the clicked node
    sig.bind('clickNode', function (e) {
        console.log(e, e.data);
        sig.graph.nodes().forEach(function (d) {
            if (d.id != e.data.node.id) d.size = nodeSize
        })
        // Use the double of node size to avoid
        // Icreasint the node size with clicked many times
        e.data.node.size = nodeSize * 2; 
        getRegionInfo(e.data.node);
        sig.refresh();
    })
    sig.renderers[0].domElements.mouse.style.position = "relative";

    force = d3.layout.force()                 
                .nodes(sig.graph.nodes())
                .links(sig.graph.edges())
                .size([maxX, maxY])
                .linkDistance(10)
                .linkStrength(0.2)
                .gravity(.2)
                .charge(-300)
                .on("tick", tick)
                .start();

   
    

    force.start();
    for (var i = n * n; i > 0; --i) force.tick();
    force.stop();

    //d3.range(sig.graph.nodes().length).forEach(function (i) {
    //    cluster(sig.graph.nodes()[i], 0.01);
    //    collide(sig.graph.nodes()[i], 0.5);
    //})

    sig.refresh();

    // TODO: if didn't use it in the future remove it.
    function tick(e) {

        if (e.alpha <= 0.005) {
            force.stop();
            return;
        }

        d3.range(sig.graph.nodes().length).forEach(function (i) {
            cluster(sig.graph.nodes()[i], 10 * e.alpha * e.alpha);
            collide(sig.graph.nodes()[i], 0.5);
        })

        sig.refresh();
    };

    // Move d to be adjacent to the cluster node.
    function cluster(d, alpha) {
        //return function(d) {
        var cluster = clusters[d.cluster];
        var x = d.x - cluster.x,
            y = d.y - cluster.y,
            l = Math.sqrt(x * x + y * y),
            r = d.size + 10;
        if (l != r) {
            l = (l - r) / l * alpha;
            d.x -= x *= l;
            d.y -= y *= l;
            cluster.x += x;
            cluster.y += y;
        }
        //};
    };

    // Resolves collisions between d and all other circles.
    function collide(d, alpha) {
        var quadtree = d3.geom.quadtree(sig.graph.nodes());
        //return function(d) {
        var r = d.size + maxRadius + Math.max(padding, clusterPadding),
            nx1 = d.x - r,
            nx2 = d.x + r,
            ny1 = d.y - r,
            ny2 = d.y + r;
        quadtree.visit(function (quad, x1, y1, x2, y2) {
            if (quad.point && (quad.point !== d)) {
                var x = d.x - quad.point.x,
                    y = d.y - quad.point.y,
                    l = Math.sqrt(x * x + y * y),
                    r = d.size + quad.point.size + (d.cluster === quad.point.cluster ? padding : clusterPadding);
                if (l < r) {
                    l = (l - r) / l * alpha;
                    d.x -= x *= l;
                    d.y -= y *= l;
                    quad.point.x += x;
                    quad.point.y += y;
                }
            }
            return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
        });
        //};
    }           
    //sigma.plugins.dragNodes(sig, sig.renderers[0]);
       
}

InitializeGraph = function () {

    PetNetwidth = $(".netDisplay").width()-15;
    PetNetheight = $(".netDisplay").height()-15;
    
    $(".netDisplay").on("mouseover", function () {
		$("#btn-group-network").removeClass('hidden');
        //$("div.toolbar-network").removeClass("hidden")
    })
    $(".netDisplay").on("mouseout", function () {
				$("#btn-group-network").addClass('hidden');
                    //$("div.toolbar-network").addClass("hidden")
                })

    netTip = d3.tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html(function (d) {

          if (d.hasOwnProperty("chr")) {
              return "<strong>Chromosome:</strong> <span style='color:red'>" + d.chr + "</span><br/>" +
                     "<string>Start: </string> <span style='color:red'>" + d.start + "</span><br/>" +
                     "<string>End: </string> <span style='color:red'>" + d.end + "</span>";
          }
          else if (d.hasOwnProperty("freq")) {
              return "<strong>Frequency:</strong> <span style='color:red'>" + d.freq + "</span>";
          };
      });
  
    force = d3.layout.force()
    				.size([PetNetwidth, PetNetheight])
                    .charge(-20)
                    .linkDistance(10);
}


getRegionInfo = function (d) {

    $("GeneInfoContainer").css("height","100%");
    //Clear previous results if any
    var RegionInfoPanel = $("#GeneInfoContainer > .infopanel");
    RegionInfoPanel.empty()

    if ($("div.chrDiv").length) { $("div.chrDiv").remove(); }

    if ($("div#svgHolder").hasClass("hidden")) {
        displayDallianceBrowser();
    }
    try {
        dalliance.chr = d.chr
        dalliance.viewStart = parseInt(d.start) - 2500
        dalliance.viewEnd = parseInt(d.end) + 500
        dalliance.highlightRegion(d.chr, parseInt(d.start), parseInt(d.end))
        dalliance.refresh()
    }
    catch (err) { }


    var regionInfoURL = "/getGeneInfo/" + genome + "/" + d.chr + "/" + 1 + "/" + d.start + "/" + d.end;
    d3.json(regionInfoURL, function (error, data) {
        //Check if we have the genes field
        if (data.hasOwnProperty("genes")) {            
            //var chrData = { "chrLength": chrLength[d.chr], "bands": chrCytobands[d.chr], "centers": chrCenters[d.chr] }
            var parent = "#GeneInfoContainer > .infopanel"

            if ($("#svgHolder .dalliance .btn-toolbar").length) {
                parent = "#svgHolder .dalliance .btn-toolbar"
            }

            DisplayChromosome(chromInfo[d.chr], parent, d, 20)
            var infoMenu = $('<div class="panel-group" id="regionInfo_accordion">');
            infoMenu.appendTo(RegionInfoPanel)
            CreateRegionInfoPanel(infoMenu, d, "regionInfo_accordion")
            CreateGeneInfoPanel(infoMenu, data.genes, "regionInfo_accordion")
        }
    }
   )
};


DisplayChromosome = function (data, parent, region, height) {

    
    var width = $(parent).width();
    if(width == 0){
        width  = $(parent).parent().width();
    }

    var chrDrawZone = d3.select(parent)
                        .append("div")
                        .attr("class", "chrDiv")
                        .attr("width", width)
                        .attr("height", height + 10)
                        .append("svg")
                        .attr("width", width)
                        .attr("height", height + 10)
                        .append("g")
                        .attr("class", "chromSVG")
                        .attr("stroke", "black")
                        .attr('transform', "translate(0, 5)")


    var scaleX = d3.scale.linear()
                   .domain([0, data.chrLength])
                   .range([40, width - 20]);

    chrDrawZone.append("text")               
               .style("font-family", "helvetica, arial, sans-serif")
               .style("font-size", "14px")
               .attr("x", 0)
               .attr("y",12)
               .text("chr" + data.chr)

    var chrRect = chrDrawZone.selectAll(".chr")
               .data(data.bands);

    chrRect.enter().append("rect")
           .attr("class", function (d) { return "chr " + d.fill })
           .attr("height", height)
           .attr("width", function (d) { return scaleX(d.end) - scaleX(d.start) })
           .attr("x", function (d) { return scaleX(d.start) })
           .attr("y", 0)



    var chrCetro = d3.select(".chromSVG").selectAll(".centro")
                   .data(data.centers);

    chrCetro.enter().append("path")
            .attr("class", function (d) { return "centro " + d.fill; })
            .attr("d", function (d) {
                if (d.fill === "acen1") {
                    return "M " + scaleX(d.start) + " 0 L " + scaleX(d.end) + " 10 L " + scaleX(d.start) + " 20 Z";
                }
                else {
                    return "M " + scaleX(d.start) + " 10 L " + scaleX(d.end) + " 0 L " + scaleX(d.end) + " 20 Z";
                }
            })

    //mark the gene region in
    chrDrawZone.append("svg:rect")
               .attr("calss", "highlight_region")
               .attr("height", height + 5)
               .attr("width", scaleX(region.end) - scaleX(region.start))
               .attr("x", scaleX(region.start))
               .attr("y", 0)
               .attr("stroke", "transparent")
               .attr("fill", "yellow")
               .attr("fill-opacity", 0.5)

}


displalAllChromosome = function (zone) {

    var height = $(zone).height()
    var width = $(zone).width();

    var chrZone = d3.select(zone).append("div")
                    .attr("class", "allChrZone")
                    .attr("heigh", height - 20)
                    .attr("width", width - 20)
                    .append("g")
                    .attr("transform", "translate(0,0)");


    chrZone.selectAll(".dispChr")
           .data(chromInfo);

    var YChrScale = d3.scale.ordinal()
                      .rangeRoundBands([20, width - 20])
                      .domain(frequencies.map(function (d) { return d.chr }));

    for (var i = 0; i < chromInfo.length; i++) {
        //DisplayChromosome()
    }
    //TO DO : Complete the function, maybe it will be substituted by the circos map
};

redraw = function () {
    if (!d3.event.sourceEvent.ctrlKey) {
        d3.select("svg.circosDiv > g.circosDiv_g").attr("transform",
            "translate(" + d3.event.translate + ")" +
            " scale(" + d3.event.scale + ")");            

        d3.select("svg.circosDiv > g.brush").attr("transform",
             "translate(" + d3.event.translate + ")" +
            " scale(" + d3.event.scale + ")");
    }
}


DrawCircosMap = function (zone) {

    var gap = 0.08726; // equivalent to 5 degrees
    var TotalChrLenghts = 0;

    var FilledDegrees = 2 * Math.PI - gap * chromInfo.length;

    var chrStarts = {};

    for (var chr in chromInfo) {
        if (chromInfo.hasOwnProperty(chr)) {
            if (chromInfo[chr] != undefined) {
                chrStarts[chr] = { "start": TotalChrLenghts, "end": TotalChrLenghts + chromInfo[chr].chrLength }
                TotalChrLenghts += chromInfo[chr].chrLength;
            }
        }
    }

    var DegreeScale = d3.scale.linear()
                   .range([0, FilledDegrees])
                   .domain([0, TotalChrLenghts]);


    var startDegree = 0;
    //To get a square zone
    var width = $(zone).width();
    var height = $(zone).height();
    var size = Math.min(width, height);

    $(zone).empty();

    // Create the brush
    brushFct = d3.svg.brush()
      .x(d3.scale.identity().domain([0, width]))
      .y(d3.scale.identity().domain([0, height]))
      .on("brushstart", brushstart)
      .on("brush", brushmove)
      .on("brushend", brushend);

    function brushstart() { };
    function brushmove() { };

    //When the brushing is finished, get the interacting
    // edges and display then on the network
    function brushend() {

        var centerX = width / 2
        var centerY = 0.85 * height / 2
        var extnt = brushFct.extent()

        extnt[0][0] = extnt[0][0] - centerX
        extnt[1][0] = extnt[1][0] - centerX
        extnt[0][1] = extnt[0][1] - centerY
        extnt[1][1] = extnt[1][1] - centerY

        var rectRec = new Path()
        rectRec.parseData("M" + extnt[0][0] + " " + extnt[0][1] + " L " + extnt[1][0] + " " +
                          extnt[0][1] + " L " + extnt[1][0] + " " + extnt[1][1] +
                          " L " + extnt[0][0] + " " + extnt[1][1] + " Z")
        var linksToDisplay = Array()
        var NodesToDisplay = Array()
        interactionsChords.classed("highlighted", function (d, i) {
            var inter = Intersection.intersectShapes(chordshapes[i], rectRec)

            if (inter.points.length > 0) {
                linksToDisplay.push(i)
                NodesToDisplay.push(d.source)
                NodesToDisplay.push(d.target)

                return true;
            }
            else {
                return false;
            }
        })

        var network = {}
        network.nodes = graph.nodes.filter(function (d) { return $.inArray(d.id, NodesToDisplay) != -1 })
        network.edges = graph.edges.filter(function (l, i) { return $.inArray(i, linksToDisplay) != -1 })        
        drawNetCanvas(network, 'div#NETContainer')
        d3.select(this).call(d3.event.target);
    }

    var svg = d3.select(zone)
                  //.append("div")
                  .append("svg")
                  .attr("width", width - 10)
                  .attr("height", height + 30)
                  .attr('class', 'circosDiv')
                  .attr('xmlns','http://www.w3.org/2000/svg')                  
                  .call(d3.behavior.zoom().scaleExtent([0.1, 50]).on("zoom", redraw));


    circosMap = svg.append("g")
                   .attr("class", "circosDiv_g");

    var brush = svg.append("g")
                   .attr("class", "brush")
                   .call(brushFct);

    // store the reference to the original handler
    var oldMousedown = brush.on('mousedown.brush');

    brush.on('mousedown.brush', function () {
        if (d3.event.ctrlKey) {
            oldMousedown.call(this);
        }
        else {
            d3.event.stopPropagation()
        }

    });


    var ourerRadius = size * .40;
    var innerRadius = size * .35;

    var gappedChromStart = Array()

    for (var chr = 1; chr < chromInfo.length; chr++) {
        gappedChromStart[chromInfo[chr].chr] = {
            "start": DegreeScale(chrStarts[chromInfo[chr].chr].start) + (chr - 1) * gap,
            "end": DegreeScale(chrStarts[chromInfo[chr].chr].end) + (chr - 1) * gap
        }
    }


    var drawArc = d3.svg.arc()
                       .innerRadius(innerRadius).outerRadius(ourerRadius)
                       .startAngle(function (d, i) { return gappedChromStart[d.chr].start })
                       .endAngle(function (d, i) { return gappedChromStart[d.chr].end })


    //Create a converter for each chromosome
    var InsideChromScale = {}

    for (var chr = 1; chr < chromInfo.length; chr++) {
        InsideChromScale[chromInfo[chr].chr] = d3.scale.linear()
                                         .domain([0, chromInfo[chr].chrLength])
                                         .range([0, DegreeScale(chrStarts[chromInfo[chr].chr].end) - DegreeScale(chrStarts[chromInfo[chr].chr].start)]);

    }

    var mainChroms = d3.select(".circosDiv_g").selectAll(".circos_chr")
                           .data(chromInfo.slice(1, chromInfo.length))

    //First draw the chromosoms main boundaries
    mainChroms.enter()
           .append("g")
           .attr("class", "main")
           .attr("transform", "translate(" + width / 2 + "," + 0.85 * height / 2 + ")")
           .append("path")
           .style("fill", "transparent")
           .style("stroke", "black")
           .attr("d", drawArc);

    //Then draw the cytobands
    mainChroms.selectAll(".circos_cyto")
                 .data(function (d) { return d.bands.concat(d.centers); })
                 .enter()
                 .append("path")
                 //.attr("class", function (d) { return "circos_cyto " + d.fill })
                 .style("fill", function(d) { return getCytoBandColor(d.fill)})
                 .style("stroke", "transparent")
                 .attr("d", d3.svg.arc()
                            .innerRadius(innerRadius).outerRadius(ourerRadius)
                            .startAngle(function (d) {
                                var parent = d3.select(this.parentNode).datum()
                                return gappedChromStart[parent.chr].start + InsideChromScale[parent.chr](d.start)
                            })
                            .endAngle(function (d, i) {
                                var parent = d3.select(this.parentNode).datum()
                                return gappedChromStart[parent.chr].start + InsideChromScale[parent.chr](d.end)
                            })
                    )


    //Draw the chromosom names
    mainChroms.selectAll(".chrName")
              .data(function (d) { return { "chr": d.chr, "angel": (gappedChromStart[d.chr].start + gappedChromStart[d.chr].end) / 2 } })
              .enter()


    mainChroms.append("svg:text")
              .each(function (d) { d.angle = (gappedChromStart[d.chr].start + gappedChromStart[d.chr].end) / 2 })
              .attr("dy", ".35em")
              .style("font-family", "helvetica, arial, sans-serif")
              .style("font-size", "9px")
              .attr("text-anchor", function (d) { return d.angle > Math.PI ? "end" : null; })
              .attr("transform", function (d) {
                  return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")"
                      + "translate(" + (ourerRadius + 5) + ")"
                      + (d.angle > Math.PI ? "rotate(180)" : "");
              })
              .text(function (d) { return "chr" + d.chr; });

    //These functions imported from the d3js library
    var arc = function (radius, startPos, degree) {
        return "A" + radius + "," + radius + " 0 " + +(degree > Math.PI) + ",1 " + startPos;
    }
    var curve = function (r0, p0, r1, p1) {
        return "Q 0,0 " + p1;
    }

    var equals = function (a, b) {
        return a.a0 == b.a0 && a.a1 == b.a1;
    }

    //Inspired from the D3js library with some modifications
    var chrod = function (radius, region1, region2) {
        var a0 = gappedChromStart[region1.chr].start + InsideChromScale[region1.chr](region1.start) - Math.PI / 2
        var a1 = gappedChromStart[region1.chr].start + InsideChromScale[region1.chr](region1.end) - Math.PI / 2

        var s = {
            a0: a0,
            a1: a1,
            p0: [radius * Math.cos(a0), radius * Math.sin(a0)],
            p1: [radius * Math.cos(a1), radius * Math.sin(a1)]
        }

        a0 = gappedChromStart[region2.chr].start + InsideChromScale[region2.chr](region2.start) - Math.PI / 2
        a1 = gappedChromStart[region2.chr].start + InsideChromScale[region2.chr](region2.end) - Math.PI / 2
        var t = {
            a0: a0,
            a1: a1,
            p0: [radius * Math.cos(a0), radius * Math.sin(a0)],
            p1: [radius * Math.cos(a1), radius * Math.sin(a1)]
        }

        return "M" + s.p0 + arc(radius, s.p1, s.a1 - s.a0) +
            (equals(s, t) ? curve(radius, s.p1, radius, s.p0) : curve(radius, s.p1, radius, t.p0) +
            arc(radius, t.p1, t.a1 - t.a0) +
            curve(radius, t.p1, radius, s.p0)) + "Z";
    }

    linkWidth = d3.scale.linear()
                      .range([0.1, 0.6])
                      .domain(d3.extent(graph.edges, function (l) { return parseInt(l.freq, 10); }));

    chordshapes = Array();

    var interactionsChords = d3.select(".circosDiv_g").selectAll("g.circos_Chords")
                               .data(graph.edges)
                               .enter()
                               .append("g")
                               .attr("class", "circos_Cord")
                               .attr("transform", "translate(" + width / 2 + "," + 0.85 * height / 2 + ")")
                               .append("path")
                               //.attr("fill", function (d) { return color(d.source.chr) })
                               .attr("stroke", function (d) { return color(graph.nodes[d.source].chr) })
                               .attr("stroke-width", function (d) { return linkWidth(d.freq) })
                               .attr("id", function (d, i) { return i; })
                               .attr("d", function (d, i) {
                                   var shape = new Path() // from D2.js
                                   //get the path
                                   var pathStr = chrod(innerRadius, graph.nodes[d.source],  graph.nodes[d.target])
                                   //Initialize a new path object
                                   shape.parseData(pathStr)
                                   chordshapes[i] = shape;
                                   return pathStr;
                               })
                              .call(highlight);
};

function highlight(_selection) {
    _selection
        .on("mouseover.highlight", function (d, i) {
            d3.select(this).classed({ highlighted: true });
        })
        .on("mouseout.highlight", function (d, i) {
            d3.select(this).classed({ highlighted: false });
        });
}


DisplaySelected = function () {

    d3.select("svg.circosDiv > g.brush");
}

CreateRegionInfoPanel = function (parentPanel, d, parentID) {

    var regInfoPanel = $('<div class="panel panel-primary">');
    regInfoPanel.appendTo(parentPanel)
    var childID = "RegionLocationInfo"
    CreatePanelHeader(regInfoPanel, "Region Info", parentID, childID)

    var content = '<table class="table table-condensed table-hover"><tbody> <tr>' +
        '<td><strong>Chromosome :</strong></td>' +
        '<td>' + d.chr + '</td></tr><tr>' +
        '<td><strong>Start :</strong></td>' +
        '<td>' + d.start + '</td></tr><tr>' +
        '<td><strong>End :</strong></td>' +
        '<td>' + d.end + '</td></tr></tbody></table>';

    CreatePanelBody(regInfoPanel, content, childID)

}


CreateClustersStatPanel = function (parent) {


    var content = '<table class="table table-condensed table-hover" id="clustersInfo" cellspacing="0" width="100%">' +
                  '<thead> <tr><th>ID</th><th>Size</th> <th>Promoter (%)</th> <th>Enhancer (%)</th> <th>Gene-Body</th>  </tr></thead>' +
                  '<tfoot> <tr><th>ID</th><th>Size</th> <th>Promoter (%)</th> <th>Enhancer (%)</th> <th>Gene-Body</th>  </tr></tfoot>' +
                  '</table>'

    $(parent).empty();
    $(parent).append(content)
    $(parent).css("height","100%");

    $("table#clustersInfo").dataTable({
        "processing": true,
        "pagingType": "full",
        "deferLoading": 50,
        "ajax": '/getInitClusterList',
        "columns": [
            { "data": "id" },
            { "data": "size" },
            { "data": "promoter" },
            { "data": "enhancer" },
            { "data": "geneBody" }
        ]
    });


    $("table#clustersInfo tbody").on("click", "tr", function () {
        $(this).toggleClass("active")
        //Get the ID
        var clusID = $(this).children()[0].textContent
        //Highlight all the nodes in the network that have this cluster ID

        //In the canvas context
        var  isActive = $(this).hasClass('active')
        
        sig.graph.nodes().forEach(function (d, i) {
            if (d.cluster == clusID) {
                if (isActive)
                    d.color = '#b2b2b2';
                else
                    d.color = chrColor[d.chr](d.start);
            }
            
        });
        sig.refresh();
        

        // This part was used in the SVG context
        //var strokWidth = null;
        //var addClass = false;
        //if ($(this).hasClass("active")) {
        //    strokWidth = 3;
        //    addClass = true;
        //}
        
        //var className = "circle.cluster-" + clusID;

        //d3.selectAll(className)
        //  .style("stroke-width", strokWidth)
        //  .classed("selectedClus", addClass);        
    })
}

CreateGeneInfoPanel = function (parentPanel, genes, parentID) {

    var regInfoPanel = $('<div class="panel panel-primary">');
    regInfoPanel.appendTo(parentPanel);
    //parentPanel.append('<div class="panel panel-default">');
    var childID = "NearestGenesInfo"

    CreatePanelHeader(regInfoPanel, "Nearest Genes TSS", parentID, childID)

    var content = "";
    if (genes.length > 0) {
        var HGNC_Link = "http://www.genenames.org/cgi-bin/gene_symbol_report?hgnc_id=";
        var Ensemble_Link = "http://uswest.ensembl.org/Multi/Search/Results?q=";
        var content = '<table class="table table-condensed table-hover">' +
            "<tbody> \
                    <thead> \
                      <tr>\
                        <td><strong>ENSEMBL ID</strong></td> \
                        <td><strong>HGNC_Sym</strong></td>  \
                        <td><strong>Chr</strong></td>\
                        <td><strong>Start</strong></td>\
                        <td><strong>End</strong></td>\
                     </tr>\
                    </thead>\
              <tbody>"

        for (var g = 0; g < genes.length; g++) {
            content += '<tr><td> <a href="' + Ensemble_Link + genes[g].ENSEMBLID + '">' + genes[g].ENSEMBLID + '</a>' + '</td>' +
                            '<td><a href="' + HGNC_Link + genes[g].HGNCID + '">' + genes[g].HGNCSymb + '</a></td>' +
                            '<td>' + genes[g].chr + '</td>' +
                            '<td>' + genes[g].start + '</td>' +
                            '<td>' + genes[g].end + '</td>';
        }
        content += "</tbody></table>"
    }
    else {
        content = "No TSS is near to this region"
    }

    CreatePanelBody(regInfoPanel, content, childID)
}

CreatePanelHeader = function (element, title, parentID, childID) {

    var header = $('<div class="panel-heading">')

    header.append('<h4 class="panel-title">' +
        '<a data-toggle="collapse" data-parent="#' + parentID + '" href="#' + childID + '">' +
            title +
        '</a>' +
      '</h4>' +
    '</div>');

    header.appendTo(element)
}

CreatePanelBody = function (element, content, panelID) {

    var bodypart = $('<div id="' + panelID + '" class="panel-collapse collapse in">');
    bodypart.append('<div class="panel-body">' + content + '</div></div>');

    bodypart.appendTo(element)
}

InitializeHist = function () {

    histMargin = { top: 10, right: 10, bottom: 40, left: 40 };

    histWidht = $(".ChromStat").width();
    histHeight = $(".ChromStat").height();

    InnerHeight = histHeight - histMargin.top - histMargin.bottom;


    //Scaling values on the X axis
    histXScale = d3.scale.ordinal()
                   .rangeRoundBands([0, histWidht - histMargin.left - histMargin.right], 0.1)
                   .domain(frequencies.map(function (d) { return d.chr; }));

    //Scaling values on the Y axis
    histYScale = d3.scale.linear()
                   .range([histHeight - histMargin.top - histMargin.bottom, 0])
                   .domain([0, histHeight]);

    //Axes
    xAxis = d3.svg.axis()
    .scale(histXScale)
    .orient("bottom");

    yAxis = d3.svg.axis()
        .scale(histYScale)
        .orient("left");

    //Configuring the tip
    tip = d3.tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html(function (d) {
          return "<strong>Frequency:</strong> <span style='color:red'>" + d.freq + "</span>";
      })


    //Initialized the histogram svg
    chrStat = d3.select("svg.PerChromStat")
                .attr("width", histWidht)
                .attr("height", histHeight)
                .append("g")
                .attr("transform", "translate(" + histMargin.left + "," + histMargin.top + ")")
                .call(tip);

    chrStat.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + InnerHeight + ")")
      .call(xAxis);

    chrStat.append("g")
        .attr("class", "y axis")
        .call(yAxis);
    /*.append("text")
       .attr("transform", "rotate(-90)")
       .attr("y", 6)
       .attr("dy", ".71em")
       .style("text-anchor", "end")
       .text("Frequency");*/

    bar = chrStat.selectAll(".bar")
      .data(frequencies);


    bar.enter().append("rect")
      .attr("class", "bar")
      .attr("x", function (d) { return histXScale(d.chr); })
      .attr("width", histXScale.rangeBand())
      .attr("y", function (d) { return histYScale(d.freq); })
      .attr("height", function (d) { return InnerHeight - histYScale(d.freq); })
      .style("fill", function (d) { return color(d.chr); })
    //.on('mouseover', tip.show)
    //.on('mouseout', tip.hide)



}

displayHistogram = function () {

    //Scaling values on the X axis
    histXScale = d3.scale.ordinal()
                   .rangeRoundBands([0, histWidht - histMargin.left - histMargin.right], 0.1)
                   .domain(frequencies.map(function (d) { return d.chr; }));

    //Scaling values on the Y axis
    histYScale = d3.scale.linear()
                   .range([histHeight - histMargin.top - histMargin.bottom, 0])
                   .domain([0, d3.max(frequencies, function (d) { return d.freq; })]);

    bar = chrStat.selectAll(".bar")
      .data(frequencies);

    //if new data add it
    bar.enter().append("svg:rect")
       .attr("class", "bar")
       .attr("x", function (d) { return histXScale(d.chr); })
       .attr("width", histXScale.rangeBand())
       .attr("y", function (d) { return histYScale(d.freq); })
       .attr("height", function (d) { return InnerHeight - histYScale(d.freq); })
       .style("fill", function (d) { return color(d.chr); })
    //.on('mouseover', tip.show)
    //.on('mouseout', tip.hide);

    //Animate existing data
    bar.transition()
      .duration(500)
      .attr("x", function (d) { return histXScale(d.chr); })
      .attr("width", histXScale.rangeBand())
      .attr("y", function (d) { return histYScale(d.freq); })
      .attr("height", function (d) { return InnerHeight - histYScale(d.freq); })
      .style("fill", function (d) { return color(d.chr); })


    //Remove the non used bars
    bar.exit()
       .transition()
       .duration(250)
       .attr("height", 0)
       .remove();

    bar.each(function (d) {
        $(this).qtip({
            style: { classes: "qtip-dark" },
            position: {
                corner: {
                    target: 'topMiddle',
                    tooltip: 'bottomMiddle'
                }
            },
            content: {
                text: function (event, api) {
                    return "<strong>Frequency:</strong> <span style='color:#ed7a53; font-weight:400;'>" + d.freq + "</span>";
                }
            }
        })
    })
    //Update X axis
    chrStat.select(".x.axis")
          .transition()
          .duration(750)
          .call(xAxis);

    //Update Y axis
    chrStat.select(".y.axis")
          .transition()
          .duration(750)
          .call(yAxis);

}


//Code inspired from : http://bl.ocks.org/nachocab/2759731
displayHeatmap = function () {



    //heatmapColor = d3.scale.linear().domain([-1.5, 0, 1.5]).range(["#278DD6", "#fff", "#d62728"]);
    heatmapColor = d3.scale.linear()
                     .domain(d3.extent([].concat.apply([], heatmap.interactions)))
                     .range(["#278DD6", "#d62728"]);

    textScaleFactor = 15;
    LabelsMargin = d3.max(heatmap.columnNames.map(function (colName) {
        return colName.toString().length;
    }));

    textScaleFactor = 10;

    heatmapMargin = {
        top: LabelsMargin * textScaleFactor,
        right: 4,
        bottom: 10,
        left: LabelsMargin * textScaleFactor
    };


    heatmapWidth = $(".InterHeatmap").width();
    heatmapHeight = $(".InterHeatmap").height();

    var heatmapInnerWidth = heatmapWidth - heatmapMargin.left - heatmapMargin.right;
    var heatmapInnerHeight = heatmapHeight - heatmapMargin.top - heatmapMargin.bottom;


    svgheatmap = d3.select("svg.InterHeatmap")
                .attr("width", heatmapWidth)
                .attr("height", heatmapHeight)
                .attr("id", "heatmap")
                .append("g")
                .attr("transform", "translate(" + heatmapMargin.left + "," + heatmapMargin.top + ")");

    heatmapXScale = d3.scale.ordinal().domain(heatmap.columnNames).rangeBands([0, heatmapInnerWidth]);
    heatmapYScale = d3.scale.ordinal().domain(heatmap.columnNames).rangeBands([0, heatmapInnerHeight]);

    columns = svgheatmap.selectAll(".column")
                     .data(heatmap.columnNames);

    columns.enter()
           .append("g")
           .attr("class", "column hpLabels")
           .attr("font-size", "11px")
           .attr("transform", function (d, i) {
               return "translate(" + heatmapXScale(d) + ")rotate(-90)";
           });

    columns.exit().remove();

    columns.append("text")
           .attr("x", 6)
           .attr("y", heatmapXScale.rangeBand() / 2)
           .attr("dy", "-.5em")
           .attr("dx", ".5em")
           .attr("text-anchor", "start")
           .attr("transform", "rotate(45)")
           .text(function (d, i) { return heatmap.columnNames[i]; });


    getRow = function (row) {
        var cell = d3.select(this).selectAll(".cell")
                     .data(row);

        cell.enter()
            .append("rect")
            .attr("class", "cell")
            .attr("x", function (d, i) {
                return heatmapXScale(heatmap.columnNames[i]);
            })
            .attr("width", heatmapXScale.rangeBand())
            .attr("height", heatmapYScale.rangeBand())
            .text(function (d) { return d; })
            .style("fill", function (d) { return heatmapColor(d); });

        cell.transition()
            .delay(250)
            .attr("height", heatmapYScale.rangeBand())
            .attr("width", heatmapXScale.rangeBand())
            .text(function (d) { return d; })
            .style("fill", function (d) { return heatmapColor(d); });

        cell.exit().remove();
    };

    rows = svgheatmap.selectAll(".row")
                  .data(heatmap.interactions)
                  .enter()
                  .append("g")
                  .attr("class", "row hpLabels")
                  .attr("name", function (d, i) { return heatmap.columnNames[i]; })
                  .attr("transform", function (d, i) {
                      return "translate(0," + heatmapYScale(heatmap.columnNames[i]) + ")";
                  })
                  .each(getRow);


    rows.append("text")
               .attr("x", -6)
               .attr("y", heatmapYScale.rangeBand() / 2)
               .attr("dy", ".32em")
               .attr("text-anchor", "end")
               .attr("font-size", "11px")
               .text(function (d, i) { return heatmap.columnNames[i]; });
};

displayDallianceBrowser = function () {

    $("div#svgHolder").removeClass("hidden");
}


getCytoBandColor = function(col){
    
 if(col=="gpos" || col == "gpos100") return( "rgb(0,0,0)")

 if(col =="gpos75") return("rgb(130,130,130)")

if(col == "gpos66") return( "rgb(160,160,160)" );

if(col == "gpos50") return( "rgb(200,200,200)" );

if(col == "gpos33") return( "rgb(210,210,210)" );

if(col == "gpos25") return ( "rgb(200,200,200)" )

if(col == "gvar") return( "rgb(220,220,220)" );

if(col == "gneg") return( "rgb(255,255,255)" );

if( col == "acen1" || col=="acen2") return( "rgb(217,47,39)");
if(col == "stalk") return( "rgb(100,127,164)" );

}
