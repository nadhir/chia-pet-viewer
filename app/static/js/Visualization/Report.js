﻿$.fn.toImage = function (dist) {
    $(this).each(function () {
        var svg$ = $(this);
        var width = svg$.width();
        var height = svg$.height();

        // Create a blob from the SVG data
        var svgData = new XMLSerializer().serializeToString(this);
        var blob = new Blob([svgData], { type: "image/svg+xml" });

        // Get the blob's URL
        var domUrl = self.URL || self.webkitURL || self;
        var blobUrl = domUrl.createObjectURL(blob);
        var img = new Image;                
        img.src = blobUrl;        
        //canvas = document.createElement('canvas');
        //canvas.getContext("2d").drawImage(img, 0, 0);
        dist.append(img)
        domUrl.revokeObjectURL(blobUrl)
        img.addEventListener("load", function(){
            this.style.width = width/2;
            this.style.height = height/2;
        })
        // Load the blob into a temporary image
        //$('<img />')
        //    .width(width)
        //    .height(height)
        //    .on('load', imgOnLoad)
        //    .attr('src', blobUrl);
    });
};

saveToCanvas = function (dist) {
    var svg$ = $(this);
    var width = svg$.width();
    var height = svg$.height();

    // Create a blob from the SVG data
    var svgData = new XMLSerializer().serializeToString(this); 
    var blob = new Blob([svgData], { type: "image/svg+xml" });

    // Get the blob's URL
    var domUrl = self.URL || self.webkitURL || self;
    var blobUrl = domUrl.createObjectURL(blob);
    var img = new Image;
    img.src = blobUrl;
    canvas = document.createElement('canvas');
    canvas.getContext("2d").drawImage(img, 0, 0);
    dist.append(canvas)    
    domUrl.revokeObjectURL(blobUrl)    
}

//imgOnLoad = function () {
//    try {
//        //var canvas = document.createElement('canvas');
//        dist.width(width);
//        dist.height(height);
//        var ctx = dist.getContext('2d');

//        // Start with white background (optional; transparent otherwise)
//        ctx.fillStyle = '#fff';
//        ctx.fillRect(0, 0, width, height);

//        // Draw SVG image on canvas
//        ctx.drawImage(this, 0, 0);

//        // Replace SVG tag with the canvas' image
//        dist.replaceWith($('<img />').attr({
//            src: dist.toDataURL(),
//            width: width,
//            height: height
//        }));
//    } finally {
//        domUrl.revokeObjectURL(blobUrl);
//    }
//}

$(document).ready(function () {

    $(".report").bind("click", function () {
        $("#myModal").modal("show");
    })

    var wizardLoaded = false;
    $('#myModal').on('shown.bs.modal', function (e) {
        $("div.modal-content").width($("div.modal-content").parent().width() - 20)
        if (!wizardLoaded) {
            $('#myWizard').easyWizard({
                before: beforeLoad
            });
        }
    })

    function beforeLoad(wizardObj, currentStepObj, nextStepObj) {
        if (nextStepObj.attr("data-step") == "2") {
            //loadNetworkPNG("div.report");
            getSelectedParts()
        }
    }

    function loadNetworkPNG(parent) {
        var container = $(parent).append('<div class="NetImg"></div>')        

        $("svg.PETNet").toImage(container);
    }
    
    
    // JUST A TEST OF THE 3D MODEL
    $("button.3DTest").bind("click", function(){
        var scene = new THREE.Scene();
        var container = $("div.netDisplay")
        
        var camera = new THREE.PerspectiveCamera( 75, container.width() / container.height(), 0.1, 5000 );

        var renderer = new THREE.WebGLRenderer();
        renderer.setSize( container.width(), container.height() );        
        
        var loader = new THREE.JSONLoader();
        loader.load( "/getChrom3D", function(geometry){
        var material = new THREE.MeshLambertMaterial({color: 0x55B663});
        mesh = new THREE.Mesh(geometry, material);        
        scene.add(mesh);
            
        container.empty();
        container.append( renderer.domElement );
        renderer.render(scene, camera);
    });
    })

})


getSelectedParts = function(){
    var nets  = $("input[class=NetStat][checked]");
    var frequency = $("input[class=FreqStat][checked]");
    var circos = $("input[class=Circos][checked]");
    var clusStat = $("input[class=ClusStat][checked]");
    
    var reports  = $("<ul></ul>");
    if(nets.length > 0) reports.append( generateNetsReport(nets) );    
    if(circos.length > 0 ) reports.append( generateCircosReport() );
    
    
    $("div.Report-frame").append( reports);
}

generateNetsReport = function(nets){
    
    var addNodesStat= false;
    var addInterIntra = false;
    for(var i=0; i < nets.length; i++){
        if(nets[i].value == "nbNodes"){
            addNodesStat = true;
            continue;
        }
        
        if(nets[i].value == "nbInterIntra"){
           addInterIntra  = true;
            continue;
        }    
    }
    
    //TODO : Add the other statistics
    var lst = $("<li><h3>Chromatin Interactions</h3></li>");
    var netDiv = $("<div></div>").appendTo(lst);
    $("svg.PETNet").toImage(netDiv);        
    
    return(lst);
}


generateCircosReport = function(){
    var lst = $("<li><h3>Circos map</h3></li>");
    var netDiv = $("<div></div>").appendTo(lst);
    $("svg.circosDiv").toImage(netDiv);        
    
    return(lst);
}

