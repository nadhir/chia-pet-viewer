import collections;
import re;
import math;
from bx.intervals.cluster import ClusterTree;
import numpy as np;
import networkx as nx; 
# from joblib import Parallel, delayed;



class PETParser(object):
    
    """This class is responsible for parsing ChIA-PET files"""
    def __init__(self, file,organism= "hs"):
        self.petFile = file;
        self.organism = organism;
        self.nbChromPerOragnisme = {"hs":24, "mouse": 20 };        
        #self.interactions = collections.defaultdict(lambda : dict);       
    
    def ReadPET(self):
        #Create a dictionarry of ClusterTrees, one for each 
        self.interactions = nx.Graph();
        self.transInteractions = nx.Graph();
        self.IntraPET = collections.defaultdict(lambda: ClusterTree(0,0));
        self.InterPet = ClusterTree(0,-1)
        nbChrom = self.nbChromPerOragnisme[self.organism];
        self.Interheatmap = {"chromosome":collections.defaultdict(lambda: 0), 
                                "interactions": np.zeros(nbChrom *nbChrom).reshape(nbChrom,nbChrom) };
        petID=1
        interID = 1
        with open(self.petFile,'r') as pets :
            next(pets);
            for interaction in pets:
                #Each line is supposed to have at least the following fields
                # ChrLeft startLeft endLeft  ChrRight startRight  endRight                
                interaction = re.sub("X|x","23", interaction);
                interaction = re.sub("Y|y","24", interaction);
                pet = re.split("\s+", re.sub("\n|chr","",interaction))
                
                #Check if the is correct
                if(len(pet)>=7):                
                    #Interactions are inserted in tree to be clustered
                    #and inserted in an interaction table

                    pet1Name = "PET#%s.1" % (interID)                    
                    pet2Name = "PET#%s.2" % (interID) 
                    
                    self.interactions.add_node(pet1Name, {"chr": pet[0], "start": pet[1], "end": pet[2], "cluster": -1})
                    self.interactions.add_node(pet2Name, {"chr": pet[3], "start": pet[4], "end": pet[5], "cluster": -1 })
                                                            
                    
                    self.Interheatmap['interactions'][int(pet[0])-1][int(pet[3])-1]+= int(pet[6]);
                    #Just to make it symetric
                    self.Interheatmap['interactions'][int(pet[3])-1][int(pet[0])-1] = self.Interheatmap['interactions'][int(pet[0])-1][int(pet[3])-1];
                    
                    self.interactions.add_edge(pet1Name,pet2Name,{"freq": pet[6]})

                    if not int(pet[0]) in  self.transInteractions.node.keys():
                        self.transInteractions.add_node(int(pet[0]),{"chr": pet[0]})

                    if not int(pet[3]) in  self.transInteractions.node.keys():
                        self.transInteractions.add_node(int(pet[3]), {"chr": pet[3]})

                    #self.interactions [interID-1] = { 'freq': pet[6], 
                    #                                 1: {"name": pet1Name, "id": petID, "chr":  pet[0]},
                    #                                 2: {"name": pet2Name, "id": petID+1, "chr": pet[3]}
                    #                              }                        

                    if(pet[0]== pet[3]):                                        
                        self.IntraPET[pet[0]].insert(int(pet[1]),int(pet[2]),petID,bytes(pet1Name,"utf-8"),bytes(pet[0],"utf-8"))
                        self.IntraPET[pet[3]].insert(int(pet[4]),int(pet[5]),petID+1,bytes(pet2Name,"utf-8"),bytes(pet[3],"utf-8"))                        
                        self.Interheatmap["chromosome"][pet[0]]+= 1

                    else:
                        self.InterPet.insert(int(pet[1]),int(pet[2]),petID,bytes(pet1Name,"utf-8"),bytes(pet[0],"utf-8"))
                        self.InterPet.insert(int(pet[4]),int(pet[5]),petID+1,bytes(pet2Name,"utf-8"),bytes(pet[3],"utf-8"))                        
                        self.Interheatmap["chromosome"][pet[0]]+= 1
                        self.Interheatmap["chromosome"][pet[3]]+= 1
                        if(not int(pet[3]) in self.transInteractions[int(pet[0])]):
                            self.transInteractions.add_edge(int(pet[0]), int(pet[3]), {"freq": 1})
                        else:
                            self.transInteractions[int(pet[0])] [int(pet[3])]["freq"] +=1 

                    petID+=2
                    interID+=1

    def updateInteractions(self):

      mergedInteraction = collections.defaultdict(lambda: ClusterTree(0,0))
      
      newLabels = collections.defaultdict(str)
      newID=1
      #Get the tree associated with each chromosome
      for chr, tree in self.IntraPET.items():
            #Browser all the regions of the tree
            for start, stop, elements in tree.getregions():
                #generate the new name
                newName = self.elementsName(elements,tree)                
                mergedNames = "_".join(newName);
                mergedInteraction[chr].insert(start,stop,newID,bytes(mergedNames,"utf-8"),bytes(chr,"utf-8"))                
                #update the names in the table
                for pet in newName:
                    newLabels[pet] = mergedNames                                        
                newID+=1     
      #Update the nodes attributes some the nodes with same name get merged          
      self.interactions = nx.relabel_nodes(self.interactions,newLabels)
      
      ##This part have some problems on windows  
      #if __name__ == "__main__":
      #    Parallel(n_jobs=5, backend="threading") (delayed(self.changeAttribute)(tree) for chr, tree in mergedInteraction.items());
      
      #For the moment use the sequential version
      for chr, tree in mergedInteraction.items() :
          self.changeAttribute(tree)

      return 0;



    def changeAttribute(self,tree):
        for start, stop, elements in tree.getregions():
              for node in elements:
                  info = tree.getNode(node)
                  self.interactions.node[info[3]]["start"] = info[1]
                  self.interactions.node[info[3]]["end"] = info[2]
                  self.interactions.node[info[3]]["chr"] = info[0]
        
        #Cluster the graph
        clusters = sorted(nx.connected_components(self.interactions), key=len, reverse= True)

        self.clusInfo = [{"id":id, "size":len(clus), "promoter": "NA", "enhancer": "NA", "geneBody": "NA"} 
                          for id,clus in enumerate(clusters)]

        for clus in range(0,len(clusters)):
            for n in clusters[clus]:
                self.interactions.node[n]["cluster"] = clus
        
        ## Calculate the layout of the graph
        #positions  = nx.spectral_layout(self.interactions)
        #for id, pos in positions.items():
        #      self.interactions.node[id]["x"] = pos[0]
        #      self.interactions.node[id]["y"] = pos[1]

    '''Concates the names of the elements provided '''
    def elementsName(self,ids,tree):
        if(len(ids) <=0): return("")
      
        Names = []
        for id in ids:
            Names.append(tree.getNode(id)[3])
            #Names.append(self.interactions[pos][1]['name'] if id % 2 ==1 else self.interactions[pos][2]['name'])

        return Names

    
    
    ''' deprected not used any more  '''
    def getInteractionsJSON(self):
        
        graph={"nodes": [], "links": []}

        for intID, pets in self.interactions.items():
            graph["nodes"].append(pets[1])
            graph["nodes"].append(pets[2])
            graph["links"].append({"source":pets[1]["id"], "target": pets[2]["id"]})

        return graph;

            



