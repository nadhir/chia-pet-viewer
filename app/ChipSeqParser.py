"""
from bx.intervals.intersection import *;
from sqlalchemy import select
from sqlalchemy.sql import and_, or_
from app import db,models
import collections;
"""
from jmosaics.jmosaics import  *
import numpy as np
# from tables import *
from math import ceil, floor
import os
import tabix

class ChipSeqParser(object):
    """This class is to create a table that holds the interactions"""

    def __init__(self, file, threshold=0.001, binsize=200):
        self.threshold = threshold
        self.path = file
        self.binSize = binsize


    def getEnrichedRegions(self, cellLine, genome, promoters, enhancers, network):

        # check if the marks exist in the database
        marks = list(promoters.keys())

        signalsPath = []
        for mark in marks:
            fname = os.path.join(self.path,genome, cellLine, "processed", mark, 'signal_fit.bed.gz')
            signalsPath.append(fname)



        # if all the mark exist then get the bins
        background = np.zeros((len(marks), 0))
        foreground = np.zeros((len(marks), 0))
        regions = np.zeros(len(network), dtype=np.int32)

        ## get the bins involved in each region
        nbNode = 0
        for node in network:

            chrom = "chr"+node['chr']

            # Calculate how many bins we have
            regionSize = ceil(node['end']/self.binSize) - floor(node['start']/self.binSize)

            # Save its length
            regions[nbNode] = regionSize

            # Prepare the background and foreground signals for each signal
            bgr = np.empty((len(marks), regionSize))
            frg = np.empty((len(marks), regionSize))

            for signal in range(len(signalsPath)):
                tb = tabix.open(signalsPath[signal])
                bins = tb.query(chrom, int(node['start']), int(node['end']))

                cpt = 0
                for bin in bins:
                    bgr[signal, cpt] = bin[3]
                    frg[signal, cpt] = bin[4]
                    cpt += 1
            # Add the results the global background and foreground tables
            background = np.append(background, bgr, axis=1)
            foreground = np.append(foreground, frg, axis=1)

            nbNode += 1

        ## keep the same order or marks in the enancer and
        promPatter = np.array([promoters[x] for x in promoters.keys()])
        enhancePatter = np.array([enhancers[x] for x in promoters.keys()])

        pNodes = []
        eNodes = []
        ## Then find the enrichment pattern in these regions
        if background.shape[1] > 0 and foreground.shape[1] > 0:
            jmos = jmosaics(100)
            pattern = jmos.getPattern(regions, background, foreground)
            if pattern.shape[1] > 0:
                for region in range(pattern.shape[1]):
                    if np.all(pattern[0:, region] == promPatter):
                        pNodes.append(network[region]['id'])

                    if np.all(pattern[0:, region] == enhancePatter):
                        eNodes.append(network[region]['id'])

        return {"Promoter": pNodes, "Enhancer": eNodes}


    # def getEnrichedRegions_hdf5(self, cellLine, genome, promoters, enhancers, network):
    #
    #     # check if the marks exist in the database
    #     marks = list(promoters.keys())
    #     filePath = os.path.join(self.path, "%s.h5" % genome)
    #     h5file = openFile(filePath, mode='r')
    #
    #     signalTables = []
    #     # if the signal is not found it will throw an exception
    #     for mark in marks:
    #         tableName = "/Cell_lines/" + cellLine + "/" + mark
    #         signalTables.append(h5file.getNode(tableName))
    #
    #     # if all the mark exist then get the bins
    #     background = np.zeros((len(marks), 0))
    #     foreground = np.zeros((len(marks), 0))
    #     regions = np.zeros(len(network), dtype=np.int32)
    #
    #     ## get the bins involved in each region
    #     nbNode = 0
    #     for node in network:
    #
    #         queryParam = {'region_start': floor(int(node["start"]) / self.binSize) * self.binSize,
    #                       'region_end': ceil(int(node["end"]) / self.binSize) * self.binSize,
    #                       'chrom': "chr"+node['chr']}
    #
    #         query = """(chr == chrom) &  (start >= region_start) & (end <= region_end)"""
    #
    #
    #         ## For each bin gepytable t the background and foreground signals
    #         regionSize = (queryParam['region_end'] - queryParam['region_start']) / self.binSize
    #
    #         regions[nbNode] = regionSize
    #         bgr = np.empty((len(marks), regionSize))
    #         frg = np.empty((len(marks), regionSize))
    #
    #         #Get the index of the bins
    #         idx = signalTables[0].get_where_list(query, queryParam)
    #
    #         if(len(idx) == 0):
    #             continue
    #         # Fill in the background and foreground values for each mark
    #         for i in range(len(marks)):
    #             # Get the list f involved bins
    #             bgr[i, :] = signalTables[i].read_coordinates(idx, 'background')
    #             frg[i, :] = signalTables[i].read_coordinates(idx, 'foreground')
    #
    #             #binsInRegion = [(x['background'], x['foreground']) for x in signalTables[i].read_where(query, queryParam)]
    #             #binsInRegion = np.array(binsInRegion)
    #
    #         background = np.append(background, bgr, axis=1)
    #         foreground = np.append(foreground, frg, axis=1)
    #
    #         nbNode += 1
    #
    #     ## keep the same order or marks in the enancer and
    #     promPatter = np.array([promoters[x] for x in promoters.keys()])
    #     enhancePatter = np.array([enhancers[x] for x in promoters.keys()])
    #
    #     pNodes = []
    #     eNodes = []
    #     ## Then find the enrichment pattern in these regions
    #     if background.shape[1] > 0 and foreground.shape[1] > 0:
    #         jmos = jmosaics(100)
    #         pattern = jmos.getPattern(regions, background, foreground)
    #         if pattern.shape[1] > 0:
    #             for region in range(pattern.shape[1]):
    #                 if np.all(pattern[0:, region] == promPatter):
    #                     pNodes.append(network[region]['id'])
    #
    #                 if np.all(pattern[0:, region] == enhancePatter):
    #                     eNodes.append(network[region]['id'])
    #
    #     return {"Promoter": pNodes, "Enhancer": eNodes}