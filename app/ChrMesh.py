import numpy as np;
from scipy.spatial import Delaunay;
from scipy.spatial import qhull;
import re;
from collections import defaultdict;

class ChrMesh(object):

    def __init__(self):
       self.ChromPoints = defaultdict(lambda: np.array([])) 
       self.triangles = defaultdict(qhull.Delaunay)

    def loadCoordinate(self,fileName):

       self.ChromPoints = defaultdict(lambda: np.array([]))
       with open(fileName,'r') as ThreeDmodel :
            content =  ThreeDmodel.read();

       verticies = re.split("\n", content);

       ## Here we are starting from the second line.       
       for vertex in verticies[1:] :
           corrdinates = re.split("\t", vertex);      
           if(len(corrdinates) != 5):
               print("line %s was ignored" % vertex);
               continue;     
           x = np.array([ float(corrdinates[2]), float(corrdinates[3]), float(corrdinates[4]) ])
           if(self.ChromPoints[corrdinates[0]].size == 0):
                self.ChromPoints[corrdinates[0]] = x
           else:
                self.ChromPoints[corrdinates[0]] = np.row_stack( (self.ChromPoints[corrdinates[0]], x ))

    
    def Triangulize(self):
       self.triangles = defaultdict(qhull.Delaunay)
       if(len(self.ChromPoints) > 0):
           for chr in self.ChromPoints:
               self.triangles[chr] =  Delaunay(self.ChromPoints[chr])

       return self.triangles