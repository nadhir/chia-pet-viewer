import os
import tabix

class TSSAnnotParser(object):
    """This class looks for TSS near to a genomic location"""    
    
    def __init__(self,genome, path):

        #AvailableGenomes = [g for g in os.listdir(path) if os.path.isdir(path + "/" + g)]
        #if genome not in AvailableGenomes:
        #    raise ValueError("The provided genome is not available")
        self.genome = genome
        self.path = path


    def GetNearByTSS(self,region, distance = 2500):

        fname = os.path.join(self.path, self.genome, "%s.tss.gz" % self.genome)

        tb = tabix.open(fname)
        nearTSS = tb.query(region['chrom'], region['region_start'], region['region_end'])
        Result = {"genes": []}

        for gene in nearTSS:
            info = {"chr": gene[0], "start": gene[1],
                    "end": gene[2], "ENSEMBLID": gene[4],
                    "HGNCID": gene[6], "HGNCSymb": gene[7]
                    }
            Result["genes"].append(info)

        return Result

    # def GetNearByTSS_hdf5(self,region, distance = 2500):
    #
    #     fname = os.path.join(self.path, "%s.h5" % self.genome)
    #     genomeFile = openFile(fname, mode="r")
    #
    #     tssTable = genomeFile.root.TSS
    #
    #     negStrandCon = """(strand == 1) & (chr == chrom) &  ((abs( (region_start + region_end)/2 - (start - 2000 + end)/2) / ( (region_start + region_end)/2 - region_start + (start - 2000 + end )/2 - end + 2500 ) ) <=1)"""
    #     posStrandCon = """(strand == 1) & (chr == chrom) &  ((abs( (region_start + region_end)/2 - (start + 2000 + end)/2) / ( (region_start + region_end)/2 - region_start + (start + 2000 + end )/2 - start + 500 ) ) <=1)"""
    #     noStrandCon= """(chr == chrom) &  ((abs( (region_start + region_end)/2 - (start + 2000 + end)/2) / ( (region_start + region_end)/2 - region_start + (start + 2000 + end )/2 - start + 500 ) ) <=1)"""
    #
    #     if region['strand'] == -1 or region['strand'] == "-":
    #         nearTSS = [x for x in tssTable.read_where(negStrandCon, region)]
    #     else:
    #         if region['strand'] == 1 or region['strand'] == "+":
    #             nearTSS = [x.fetch_all_fields() for x in tssTable.read_where(posStrandCon, region)]
    #         else:
    #             nearTSS = [x.fetch_all_fields() for x in tssTable.read_where(noStrandCon, region)]
    #
    #     Result = {"genes": []}
    #
    #     for gene in nearTSS :
    #         info = {"chr": gene['chr'].decode('utf-8') , "start": str(gene['start']), "end": str(gene['end']),
    #                 "HGNCID": str(gene['HGNCID']),"HGNCSymb": gene['HGNCSymb'].decode('utf-8'),
    #                 "ENSEMBLID": gene['Ensembl'].decode('utf-8')}
    #         Result["genes"].append(info)
    #
    #     return Result