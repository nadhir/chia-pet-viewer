from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.compress import Compress
import numpy as np


app = Flask(__name__)
app.config.from_object('config')
Compress(app)

from . import views