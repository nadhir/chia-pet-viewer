from app import db
#from sqlalchemy import create_engine, db.ForeignKey
#from sqlalchemy import db.Column, Integer, db.String, DECIMAL, FLOAT, db.BIGINT
#from sqlalchemy.orm import db.relationship, sessionmaker
#from sqlalchemy.ext.declarative import declarative_db.Model
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from sqlalchemy import func

class Genome(db.Model):
    __tablename__ = "genome"
    id= db.Column(db.Integer, primary_key= True)
    name = db.Column("name",db.String)
    cellLines = db.relationship('CellLine')
    #bins = db.relationship('Bin')

class EnrichedFor(db.Model):
    __tablename__="EnrichedFor"
    enrichID = db.Column(db.Integer, primary_key=True)
    cellLineID = db.Column(db.Integer, db.ForeignKey('CellLine.id'))
    binID = db.Column(db.Integer, db.ForeignKey('Bin.id')) 
    signalID = db.Column(db.Integer, db.ForeignKey('SingalType.id'))
    background = db.Column("background", db.FLOAT)
    foreground = db.Column("foreground", db.FLOAT)

class CellLine(db.Model):
    __tablename__ = "CellLine"
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column("name",db.String(15))    
    genomeId  = db.Column(db.Integer, db.ForeignKey("genome.id"))

class SignalType(db.Model):
    __tablename__ = "SingalType"
    id= db.Column(db.Integer, primary_key= True)
    name = db.Column("name",db.String(40))

class Bin(db.Model):
    __tablename__ = "Bin"
    id = db.Column(db.Integer, primary_key=True)
    chr = db.Column("chr",db.String(5))
    start = db.Column("start",db.BIGINT)
    end = db.Column("end",db.BIGINT)
    genomeId  = db.Column(db.Integer, db.ForeignKey("genome.id"))    


class TSS(db.Model):
    __tablename__ = "TSS"
    id = db.Column(db.Integer, primary_key = True)
    genomeID = db.Column("genomeID", db.ForeignKey("genome.id"))
    chr = db.Column("chr",db.String(5))
    start = db.Column("start",db.BIGINT)
    end = db.Column("end",db.BIGINT)
    strand = db.Column("strand",db.Integer)
    ensemblID = db.Column("ensemblID",db.String)
    HGNCID = db.Column("HGNCID",db.Integer)
    HGNCSymb = db.Column("HGNCSymb",db.String)
    @hybrid_method
    def intersects(self,interval, upStrm, downStrm):
        muA = (int(interval.start) + int(interval.end))/2
        roA =  muA - int(interval.start)
        if(interval.strand == -1 or interval.strand == "-"):
            muB = (interval.end + upStrm +  interval.end - downStrm)/2
            roB = muB - interval.end + downStrm
        else:
            muB = (interval.start - upStrm +  interval.start + downStrm)/2
            roB = muB - interval.start + upStrm
        gapRatio = abs(muA - muB)/ (roA + roB)
        return gapRatio < 1


