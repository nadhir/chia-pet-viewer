import os
UPLOAD_FOLDER="./tmp";
SECRET_KEY="#@#$$%TRECE#$#@&H^||";
basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'static\\db\\bio3.db')
GENOMES= os.path.join(basedir, 'static', 'Peaks')
ANNOT= os.path.join(basedir, 'static', 'Annotations')
BIN_SIZE=200
